#!/usr/bin/python3.4
""""""
import sys, subprocess, os, stat
from Bio import SeqIO
from Bio import AlignIO
from Bio import SeqRecord
from Bio.Align import AlignInfo
from Bio.Align.Applications import ClustalwCommandline



def main():
    # We want to know:
    #   Of the input fastq files:
    #   - The amount of sequences in the forward fastq.[check]
    #   - The amount of sequences in the forward fastq. [check]
    #   - The number of sequences that do not have an antagonist. [check]
    #   - The number of sequences that do have an antagonist.
    #   Of the antagonist pairs:
    #   - The average score.[check]
    #   - The lengths of both sequences. [check]
    #   - The length of the alignment and thus, the consensus. [check]
    #   - The number of dashes in both sequences. [check]
    #   - The number of SNP nucleotides in both sequences. [check]
    #   - The number of times there is a conflict with equal nucleotides. [check]
    #       - The number of SNPs that overlap. [check]
    #           - Of those the average difference in score. [check]
    #   - The number of times there is a conflict with non-equal nucleotides. [check]
    #       - The number of times SNPs are involved. [check]
    #           - The number of times the SNP is chosen for consensus in those cases. [check]
    #               - The average difference in score in such cases. [check]



    # Fastq inputfiles from the commandline.
    forwardFile = sys.argv[1]
    reverseFile = sys.argv[2]

    output = open("/".join(sys.argv[1].split("/")[:-2]) + "/Consensus.fastq", mode="w")

    # Parse the fastq files and transform to dictionary with [id]=SeqRecord.
    forwardRecordDict = SeqIO.to_dict(SeqIO.parse(forwardFile, "fastq"))
    reverseRecordDict = SeqIO.to_dict(SeqIO.parse(reverseFile, "fastq"))

    # Number of sequences in both files.
    numOfForward = len(forwardRecordDict)
    numOfReverse = len(reverseRecordDict)

    # counter for number of pairs, i.e. one forward and one reverse for a sample.
    numOfPairs = 0

    # We also need a container to store information of the pairs.
    container = []

    # Check if for a forward sample there is a reverse one.
    for key, value in forwardRecordDict.items():

        # Forward and reverse files do have the same filename only difference being the letter "R" and "F".
        reverseKey = key.replace("F", "R")
        if reverseKey in reverseRecordDict:
            numOfPairs += 1
            reverseRecord = reverseRecordDict[reverseKey]
            forwardRecord = value

            # Make the reverse complement of the reverse sequence, keeping all the info of the SeqRecord object.
            reverseRecordReversed = reverseRecord.reverse_complement(id=True, name=True, description=True,features=True,
                                                                     annotations=True,letter_annotations=True,
                                                                     dbxrefs=True)

            # Open a handle and write the seqrecord to file in fasta format for use in msa program.
            handle = open("/tmp/PWsequences.fa", mode="w")
            SeqIO.write([forwardRecord, reverseRecordReversed], handle, "fasta")
            handle.close()

            # Use clustalw to align both sequences.
            with open("/tmp/log.log", mode="a") as log:
                cmd = subprocess.call(["clustalw", "-infile=/tmp/PWsequences.fa", "-outfile=/tmp/PWout.fa",
                                       "-output=FASTA"], stdout=log, stderr=log)

            # Read the sequence alignment.
            alignment = AlignIO.read("/tmp/PWout.fa", "fasta")

            # Make a dictionary for information storage.
            pairInfo = {}
            consensus = ""
            consensusScore = []
            dashCounter1 = 0
            dashCounter2 = 0
            forwardHetCounter = 0
            reverseHetCounter = 0
            afterHetCounter = 0
            nonEqualConflict = 0
            nonEqualConflictHet = 0
            nonEqualConflictHetChosen = 0
            nonEqualConflictHetDifSum = 0
            nonEqualConflictHetChosenNoDif = 0
            equalConflict = 0
            equalConflictHet = 0
            equalConflictHetDifSum = 0
            pairInfo["ForwardLength"] = len(forwardRecord.seq)
            pairInfo["ReverseLength"] = len(reverseRecordReversed.seq)
            pairInfo["ForwardAvgScore"] = sum(forwardRecord.letter_annotations["phred_quality"])/len(forwardRecord.seq)
            pairInfo["ReverseAvgScore"] = sum(reverseRecordReversed.letter_annotations["phred_quality"])/\
                                          len(reverseRecordReversed.seq)
            pairInfo["ConsensusLength"] = len(alignment[0].seq)

            # Create a consensus and store the info.
            for i, nucleotide in enumerate(alignment[0].seq):
                nucs = {"A", "T", "C", "G"}
                if nucleotide not in nucs and nucleotide != "-":
                    forwardHetCounter += 1
                if alignment[1].seq[i] not in nucs and nucleotide != "-":
                    reverseHetCounter += 1
                couple = [nucleotide, alignment[1].seq[i]]
                if couple[0] == "-":
                    dashCounter1 += 1
                    consensus += couple[1]
                    reverseScore = reverseRecordReversed.letter_annotations["phred_quality"][i - dashCounter2]
                    consensusScore.append(reverseScore)
                    if couple[1] not in nucs:
                        afterHetCounter += 1
                elif couple[1] == "-":
                    dashCounter2 += 1
                    consensus += couple[0]
                    forwardScore = forwardRecord.letter_annotations["phred_quality"][i - dashCounter1]
                    consensusScore.append(forwardScore)
                    if couple[0] not in nucs:
                        afterHetCounter += 1
                elif couple[0] == couple[1]:
                    equalConflict += 1
                    forwardScore = forwardRecord.letter_annotations["phred_quality"][i - dashCounter1]
                    reverseScore = reverseRecordReversed.letter_annotations["phred_quality"][i - dashCounter2]
                    consensus += couple[0]
                    tmp = [forwardScore, reverseScore]
                    consensusScore.append(sorted(tmp, reverse=True)[0])
                    if couple[0] not in nucs:
                        equalConflictHet += 1
                        equalConflictHetDifSum += (sorted(tmp, reverse=True)[0] - sorted(tmp, reverse=True)[1])
                        afterHetCounter += 1
                elif not couple[0] == couple[1]:
                    nonEqualConflict += 1
                    forwardScore = forwardRecord.letter_annotations["phred_quality"][i - dashCounter1]
                    reverseScore = reverseRecordReversed.letter_annotations["phred_quality"][i - dashCounter2]
                    if couple[0] or couple[1] not in nucs:
                        nonEqualConflictHet += 1
                    if forwardScore > reverseScore:
                        consensus += couple[0]
                        consensusScore.append(forwardScore)
                        if couple[0] not in nucs:
                            afterHetCounter += 1
                            nonEqualConflictHetChosen += 1
                            nonEqualConflictHetDifSum += (forwardScore - reverseScore)
                    elif reverseScore > forwardScore:
                        consensus += couple[1]
                        consensusScore.append(reverseScore)
                        if couple[1] not in nucs:
                            afterHetCounter += 1
                            nonEqualConflictHetChosen += 1
                            nonEqualConflictHetDifSum += (reverseScore - forwardScore)
                    elif forwardScore == reverseScore:
                        if couple[0] not in nucs:
                            consensus += couple[0]
                            consensusScore.append(forwardScore)
                            afterHetCounter += 1
                            nonEqualConflictHetChosenNoDif += 1
                        elif couple[1] not in nucs:
                            consensus += couple[1]
                            consensusScore.append(reverseScore)
                            afterHetCounter += 1
                            nonEqualConflictHetChosenNoDif += 1
                        else:
                            consensus += couple[0]
                            consensusScore.append(forwardScore)
            new = SeqRecord.SeqRecord(consensus,id=key.replace("F", "CONSENSUS"), description="" , letter_annotations={"phred_quality":consensusScore})
            pairInfo["InitialForwardHet"] = forwardHetCounter
            pairInfo["InitialReverseHet"] = reverseHetCounter
            pairInfo["ForwardDashes"] = dashCounter1
            pairInfo["ReverseDashes"] = dashCounter2
            pairInfo["RemainingHets"] = afterHetCounter
            pairInfo["NonEquelaConflicts"] = nonEqualConflict
            pairInfo["NumberOfConflictsHetsInvolved"] = nonEqualConflictHet
            pairInfo["NumberOfHetsFavoured"] = nonEqualConflictHetChosen
            pairInfo["ScoreDifWhenHetFavoured"] = nonEqualConflictHetDifSum
            pairInfo["TimesHetFavouredScoreEqual"] = nonEqualConflictHetChosenNoDif
            pairInfo["ConflictsEqual"] = equalConflict
            pairInfo["ConflictsEqualHetFavoured"] = equalConflictHet
            pairInfo["ConflictsEqualHetFavouredDif"] = equalConflictHetDifSum
            container.append(pairInfo)
            SeqIO.write(new, output, "fastq")
        else:
            SeqIO.write(value, output, "fastq")

    # OMSLACHTIG!!!!
    # OMSLACHTIG!!!!
    # OMSLACHTIG!!!!
    # OMSLACHTIG!!!!
    for key, value in reverseRecordDict.items():
        forwardKey = key.replace("R", "F")
        if not forwardKey in forwardRecordDict:
            reverseRecordReversed = value.reverse_complement(id=True, name=True, description=True,
                                                                     features=True,
                                                                     annotations=True, letter_annotations=True,
                                                                     dbxrefs=True)
            SeqIO.write(reverseRecordReversed, output, "fastq")




if __name__ == "__main__":
    main()
    print("Finished")
