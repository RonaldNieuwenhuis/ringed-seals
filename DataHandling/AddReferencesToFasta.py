import os, sys
from Bio import SeqIO

def main():
	for locusfolder in os.listdir("/home/ronald/Documenten/Internship/Data/Output4"):
		with open("/home/ronald/Documenten/Internship/Data/Output4/" + locusfolder + "/Consensus.fastq") as handle:
			with open("/home/ronald/Documenten/Internship/Data/References/" + locusfolder + ".fa") as handle2:
				reference = SeqIO.read(handle2, "fasta")
				records = list(SeqIO.parse(handle, "fastq"))
				#records.append(reference)
				with open("/home/ronald/Documenten/Internship/Data/Output4/" + locusfolder + "/ConsensusAndRef.qual", mode="w") as handle3:
					SeqIO.write(records, handle3, "qual")
				print(locusfolder + " Done.")
	
if __name__ == "__main__":
	main()
