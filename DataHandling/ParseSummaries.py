import sys
import ast
import csv
from Folder import Folder
import rpy2


def main(windowSize=None, threshold=None, minScore=None, inputDir=None,  outputDir=None):

    # I know this is not pythonic! But for the convenience and with the possibility of forgetting something I wrote it down.
    sequenceCountContainer1 = []
    sequenceCountContainer2 = []
    sequenceCountContainer3 = []
    sequenceCountContainer4 = []

    sequenceLengthContainer1 = []
    sequenceLengthContainer2 = []
    sequenceLengthContainer3 = []
    sequenceLengthContainer4 = []

    nCalls1 = []
    nCalls2 = []
    nCalls3 = []
    nCalls4 = []

    snpCalls1 = []
    snpCalls2 = []
    snpCalls3 = []
    snpCalls4 = []

    nucCountContainer1 = []
    nucCountContainer2 = []
    nucCountContainer3 = []
    nucCountContainer4 = []


    scoreColCountContainer1 = []
    scoreColCountContainer2 = []
    scoreColCountContainer3 = []

    scoreColLengthContainer1 = []
    scoreColLengthContainer2 = []
    scoreColLengthContainer3 = []

    avgScoreContainer1 = []
    avgScoreContainer2 = []
    avgScoreContainer3 = []

    avgFileScoreContainer1 = []
    avgFileScoreContainer2 = []
    avgFileScoreContainer3 = []


    scoreCountContainer1 = []
    scoreCountContainer2 = []
    scoreCountContainer3 = []

    if inputDir:
        cmdarg1 = inputDir
    else:
        cmdarg1 = sys.argv[1]
    folder = Folder(cmdarg1)
    if outputDir:
        cmdarg2 = outputDir
    else:
        cmdarg2 = sys.argv[2]
    outputFolder = Folder(cmdarg2)
    filesList = folder.walkThroughTreeGetFilePaths()
    fastaSumList = [x for x in filesList if x.endswith(".FASTASUMMARY.csv")]
    qualSumList = [x for x in filesList if x.endswith(".QUALSUMMARY.csv")]
    totalSequences = 0
    fastas = len(fastaSumList)
    quals = len(qualSumList)
    if not {windowSize, threshold, minScore} == {None}:
        if minScore == 0:
            fileName = "win=" + str(windowSize) + "_thres=" + str(threshold) + "_filter=0.9xavgScore" + "_"
        else:
            fileName = "win=" + str(windowSize) + "_thres=" + str(threshold) + "_filter=" + str(minScore) + "_"
    else:
        fileName = "win=NA_thres=NA_filter=NA_"
    for fastaSum in fastaSumList:
        with open(fastaSum) as handle:
            allLines = handle.read()
            allLines = allLines.split("\n")
            for i in range(3, len(allLines)-1):
                try:
                    line = allLines[i]
                    line = line.split("|")
                except:
                    break
                numberOfsequences = int((len(line) - 5)/3)
                nonSnp = {"N", "A", "T", "C", "G"}
                if i == 3:
                    sequenceCountContainer1.append(int(line[1]))
                    sequenceLengthContainer1 += [int(x) for x in line[2:2+numberOfsequences] if x != ""]
                    nucCountContainer1.append(int(line[-3]))
                    compDict = ast.literal_eval(line[-2])
                    nCalls1.append(sum([value for key, value in compDict.items() if key == "N"]))
                    snpCalls1.append(sum([value for key, value in compDict.items() if key not in nonSnp]))
                if i == 4:
                    sequenceCountContainer2.append(int(line[1]))
                    sequenceLengthContainer2 += [int(x) for x in line[2:2+numberOfsequences] if x != ""]
                    nucCountContainer2.append(int(line[-3]))
                    compDict = ast.literal_eval(line[-2])
                    nCalls2.append(sum([value for key, value in compDict.items() if key == "N"]))
                    snpCalls2.append(sum([value for key, value in compDict.items() if key not in nonSnp]))
                if i == 5:
                    sequenceCountContainer3.append(int(line[1]))
                    sequenceLengthContainer3 += [int(x) for x in line[2:2+numberOfsequences] if x != ""]
                    nucCountContainer3.append(int(line[-3]))
                    compDict = ast.literal_eval(line[-2])
                    nCalls3.append(sum([value for key, value in compDict.items() if key == "N"]))
                    snpCalls3.append(sum([value for key, value in compDict.items() if key not in nonSnp]))
                if i == 6:
                    sequenceCountContainer4.append(int(line[1]))
                    sequenceLengthContainer4 += [int(x) for x in line[2:2+numberOfsequences] if x != ""]
                    nucCountContainer4.append(int(line[-3]))
                    compDict = ast.literal_eval(line[-2])
                    nCalls4.append(sum([value for key, value in compDict.items() if key == "N"]))
                    snpCalls4.append(sum([value for key, value in compDict.items() if key not in nonSnp]))
    for qualSum in qualSumList:
        with open(qualSum) as handle:
            allLines = handle.read()
            allLines = allLines.split("\n")
            for i in range(3, len(allLines)-1):
                try:
                    line = allLines[i]
                    line = line.split("|")
                except:
                    break
                numberOfsequences = int((len(line) - 5)/2)
                if i == 3:
                    scoreColCountContainer1.append(int(line[1]))
                    scoreColLengthContainer1 += [int(x) for x in line[2:2+numberOfsequences] if x != ""]
                    avgScoreContainer1 += [float(x) for x in line[3 + numberOfsequences:(3 + 2 * numberOfsequences)] if x != ""]
                    avgFileScoreContainer1.append(line[-2])
                    scoreCountContainer1.append(line[-1])
                if i == 4:
                    scoreColCountContainer2.append(int(line[1]))
                    scoreColLengthContainer2 += [int(x) for x in line[2:2+numberOfsequences] if x != ""]
                    avgScoreContainer2 += [float(x) for x in line[3 + numberOfsequences:(3 + 2 * numberOfsequences)] if x != ""]
                    avgFileScoreContainer2.append(line[-2])
                    scoreCountContainer2.append(line[-1])
                if i == 5:
                    scoreColCountContainer3.append(int(line[1]))
                    scoreColLengthContainer3 += [int(x) for x in line[2:2+numberOfsequences] if x != ""]
                    avgScoreContainer3 += [float(x) for x in line[3 + numberOfsequences:(3 + 2 * numberOfsequences)] if x != ""]
                    avgFileScoreContainer3.append(line[-2])
                    scoreCountContainer3.append(line[-1])
    with open(str(outputFolder) + "/" + fileName + "1.csv", mode="w") as handle:
        writer = csv.writer(handle, delimiter=',', quoting=csv.QUOTE_NONNUMERIC, dialect="excel", escapechar='\\')
        writer.writerow(["Number of sequences"] + sequenceCountContainer1)
        writer.writerow(["Number of score collections"])
        writer.writerow(["Sequence lengths"] + sequenceLengthContainer1)
        writer.writerow(["Score collection lengths"])
        writer.writerow(["Score collection average"])
        writer.writerow(["N calls"] + nCalls1)
        writer.writerow(["SNP calls"] + snpCalls1)
        writer.writerow(["Total nucleotide count"] + nucCountContainer1)
        writer.writerow(["Average file score"])
    with open(str(outputFolder) + "/" + fileName + "2.csv", mode="w") as handle:
        writer = csv.writer(handle, delimiter=',', quoting=csv.QUOTE_NONNUMERIC, dialect="excel", escapechar='\\')
        writer.writerow(["Number of sequences"] + sequenceCountContainer2)
        writer.writerow(["Number of score collections"] + scoreColCountContainer1)
        writer.writerow(["Sequence lengths"] + sequenceLengthContainer2)
        writer.writerow(["Score collection lengths"] + scoreColLengthContainer1)
        writer.writerow(["Score collection average"] + avgScoreContainer1)
        writer.writerow(["N calls"] + nCalls2)
        writer.writerow(["SNP calls"] + snpCalls2)
        writer.writerow(["Total nucleotide count"] + nucCountContainer2)
        writer.writerow(["Average file score"] + avgFileScoreContainer1)
    with open(str(outputFolder) + "/" + fileName + "3.csv", mode="w") as handle:
        writer = csv.writer(handle, delimiter=',', quoting=csv.QUOTE_NONNUMERIC, dialect="excel", escapechar='\\')
        writer.writerow(["Number of sequences"] + sequenceCountContainer3)
        writer.writerow(["Number of score collections"] + scoreColCountContainer2)
        writer.writerow(["Sequence lengths"] + sequenceLengthContainer3)
        writer.writerow(["Score collection lengths"] + scoreColLengthContainer2)
        writer.writerow(["Score collection average"] + avgScoreContainer2)
        writer.writerow(["N calls"] + nCalls3)
        writer.writerow(["SNP calls"] + snpCalls3)
        writer.writerow(["Total nucleotide count"] + nucCountContainer3)
        writer.writerow(["Average file score"] + avgFileScoreContainer2)
    with open(str(outputFolder) + "/" + fileName + "4.csv", mode="w") as handle:
        writer = csv.writer(handle, delimiter=',', quoting=csv.QUOTE_NONNUMERIC, dialect="excel", escapechar='\\')
        writer.writerow(["Number of sequences"] + sequenceCountContainer4)
        writer.writerow(["Number of score collections"] + scoreColCountContainer3)
        writer.writerow(["Sequence lengths"] + sequenceLengthContainer4)
        writer.writerow(["Score collection lengths"] + scoreColLengthContainer3)
        writer.writerow(["Score collection average"] + avgScoreContainer3)
        writer.writerow(["N calls"] + nCalls4)
        writer.writerow(["SNP calls"] + snpCalls4)
        writer.writerow(["Total nucleotide count"] + nucCountContainer4)
        writer.writerow(["Average file score"] + avgFileScoreContainer3)


if __name__ == "__main__":
    main()
    print("Finished")