# Load the library containing the argumentparser.
# sudo apt-get -y install libcurl4-gnutls-dev


if (!require("Xmisc") | !require("data.table") | !require("ggplot2") | !require("scales") | !require("devtools")) {
  install.packages("devtools", dependencies=True, repos=)
  library("devtools")
  dev_mode(on=T)
  install_github("hadley/ggplot2")
  install.packages("Xmisc", repos="http://cran.rstudio.com/")
  install.packages("data.table", repos="http://cran.rstudio.com/")
  #install.packages("ggplot2", repos = "https://github.com/hadley/ggplot2.git")
  install.packages("scales", repos = "http://cran.rstudio.com/")
  library("Xmisc")
  library("data.table")
  library("ggplot2")
  library("scales")
}

dev_mode(on=T)
folders <- c("/home/ronald/Documenten/Internship/Data/Output4/", "/home/ronald/Documenten/Internship/Data/Output7/", "/home/ronald/Documenten/Internship/Data/Output8/")#,"/home/ronald/Documenten/Internship/Data/Results3/","/home/ronald/Documenten/Internship/Data/Results4/","/home/ronald/Documenten/Internship/Data/Results5/","/home/ronald/Documenten/Internship/Data/Results6/","/home/ronald/Documenten/Internship/Data/Results7/","/home/ronald/Documenten/Internship/Data/Results8/")
print(folders[1])

ReadFiles <- function(x, y) {
  lapply(x[[1]], function(i) {container[[i]] <<- read.csv(y[[1]][which(as.array(x[[1]]) == i, arr.ind = T)], header=F, row.names = 1)
  })
}

# Make a list to store the lists in that are created by reading the 4 .csv files with read.csv.
container <- list()
for (folder in folders) {
  # List the .csv files in the directory.
  RelativeFiles <- list(list.files(path=folder, pattern="*.csv", full.names=F, recursive=FALSE))
  AbsoluteFiles <- list(list.files(path=folder, pattern="*.csv", full.names=T, recursive=FALSE))
  names <- lapply(RelativeFiles, function(x) strsplit(x, ".csv"))

  # Loop through the relative and absolute filenames list in order to open the absolute path and give it the right name.
  ReadFiles(names, AbsoluteFiles)
}

# Create a list with dataframes of the csv data. Also give them generic names for the four stages of processing in the pipeline.
# In order to transpose the data it has to be made a matrix.
GenericNames <- list()


GenericNames[["Initial"]] <- list()
for (x in which(grepl("_1", names(container)) == T)) {
  print(names(container[x]))
  GenericNames$Initial[[names(container[x])]] <- as.data.frame(t(as.matrix(container[[x]])))
}

GenericNames[["AfterTT"]] <- list()
for (x in which(grepl("_2", names(container)) == T)) {
  print(names(container[x]))
  GenericNames$AfterTT[[names(container[x])]] <- as.data.frame(t(as.matrix(container[[x]])))
}

GenericNames[["Trimmed"]] <- list()
for (x in which(grepl("_3", names(container)) == T)) {
  print(names(container[x]))
  GenericNames$Trimmed[[names(container[x])]] <- as.data.frame(t(as.matrix(container[[x]])))
}

GenericNames[["Filtered"]] <- list()
for (x in which(grepl("_4", names(container)) == T)) {
  print(names(container[x]))
  GenericNames$Filtered[[names(container[x])]] <- as.data.frame(t(as.matrix(container[[x]])))
}

for(x in names(GenericNames$Initial)) {
  GenericNames$Initial[[x]] <- cbind(GenericNames$Initial[[x]], `Process step` = "Initial")
}

for(x in names(GenericNames$AfterTT)) {
  GenericNames$AfterTT[[x]] <- cbind(GenericNames$AfterTT[[x]], `Process step` = "AfterTT")
}

for(x in names(GenericNames$Trimmed)) {
  GenericNames$Trimmed[[x]] <- cbind(GenericNames$Trimmed[[x]], `Process step` = "Trimmed")
}

for(x in names(GenericNames$Filtered)) {
  GenericNames$Filtered[[x]] <- cbind(GenericNames$Filtered[[x]], `Process step` = "Filtered")
}

for (x in names(GenericNames)){
  #print(x)
  for (i in names(GenericNames[[x]])) {
      SettingsList <- as.vector(strsplit(i, "_"))
      #print(SettingsList)
    SettingsSplit <- strsplit(SettingsList[[1]], "=")
    WindowSize <- SettingsSplit[[1]][2]
    Threshold <- SettingsSplit[[2]][2]
    FilterScore <- SettingsSplit[[3]][2]
    print(FilterScore)
    GenericNames[[x]][[i]][["Window size"]] <- WindowSize
    GenericNames[[x]][[i]][["Threshold"]] <- Threshold
    GenericNames[[x]][[i]][["Filter score"]] <- FilterScore
  }
}
AllData1 <- rbindlist(GenericNames$Initial, use.names = T, fill = T)
AllData2 <- rbindlist(GenericNames$AfterTT, use.names = T, fill = T)
AllData3 <- rbindlist(GenericNames$Trimmed, use.names = T, fill = T)
AllData4 <- rbindlist(GenericNames$Filtered, use.names = T, fill = T)

#AllData <- rbindlist(list(AllData1, AllData2, AllData3, AllData4), use.names = T, fill = T)

# col <- c("tan", "palegoldenrod", "lightcyan", "lightpink")
# 
# gh <- factor(AllData$`Window size`, levels = col, labels = col)



# ylimWindowSize <- boxplot.stats(AllData$`Sequence lengths`)$stats[c(1, 5)]
# ggplot(AllData, aes(x = `Window size`, y = `Sequence lengths`)) +
#   geom_point(position=position_jitter(width=0.3), alpha=0.2, show.legend = F, colour = gh) +
#   geom_boxplot(size=1.2, fill=NA, outlier.size=NA, show.legend = F) +
#   geom_boxplot(size=1.2, fill=NA, outlier.size=NA, color=alpha("black", 0.2), show.legend = F) + 
#   coord_cartesian(ylim = ylimWindowSize*1.15) + 
#   theme_linedraw() +
#   ggtitle("Sequence lengths for different trim window size")

