# import os
import sys
import csv
import datetime
import os
from Fasta import Fasta
from Qual import Qual
from NotAbsolutePathError import NotAbsolutePathError


class Overview:
    def __init__(self, name, *args):
        """"""
        try:
            os.path.isabs(name)
            self.fileName = name
        except:
            raise NotAbsolutePathError("The given filename " + name + " is not an absolute path.")
        if set(map(type, args)) == {Fasta}:
            self.writeFastaComparison(args)
        if set(map(type, args)) == {Qual}:
            self.writeQualComparison(args)

    def writeFastaComparison(self, *Fastas):
        """"""
        fastaList = list(Fastas[0])
        org = fastaList[0]
        sequenceNames = fastaList[0].sequenceHeaders
        with open(self.fileName, mode="w", newline='') as handle:
            writer = csv.writer(handle, delimiter='|', quoting=csv.QUOTE_NONE, dialect="excel", escapechar='\\')
            writer.writerow(['File created timestamp:', str(datetime.datetime.now())])
            l = ['File names:', 'Number of sequences in file:', 'Sequence lengths:', 'Sequence Compositions:',
                 'Percentual Sequence Compositions:', "Total Nucleotide count:", "File nucleotide composition:",
                 "File percentual nucleotide composition:"]
            insert = ["" for i in range(0, len(sequenceNames)-1)]
            l[3:3] = insert
            l[3 + len(insert) + 1:3 + len(insert) +1] = insert
            l[3 + 2 * len(insert) + 2:3 + 2 * len(insert) + 2] = insert
            writer.writerow(l)
            sequenceNames = sorted(sequenceNames)
            sequenceNames *= 3
            sequenceNames.insert(0, '')
            sequenceNames.insert(1, '')
            writer.writerow(sequenceNames)
            for x in fastaList:
                if org.numberOfSequences != x.numberOfSequences:
                    for t in org.sequenceLengths.keys():
                        if not t in x.sequenceLengths.keys():
                            x.sequenceLengths[t] = ""
                    for t in org.sequenceCompositions.keys():
                        if not t in x.sequenceCompositions.keys():
                            x.sequenceCompositions[t] = ""
                    for t in org.sequencePercentualCompositions.keys():
                        if not t in x.sequencePercentualCompositions.keys():
                            x.sequencePercentualCompositions[t] = ""
            check = [True if sorted(org.sequenceLengths.keys()) == sorted(x.sequenceLengths.keys()) else False for x
                     in fastaList]
            if set(check) == {True}:
                for x in fastaList:
                    l = [x.name, x.numberOfSequences]
                    l = l + [item[1]for item in sorted(x.sequenceLengths.items())] + \
                        [item[1] for item in sorted(x.sequenceCompositions.items())] + \
                        [item[1] for item in sorted(x.sequencePercentualCompositions.items())]
                    l = l + [x.totalNucleotideCount] + [x.fileComposition] + [x.filePercentualComposition]
                    writer.writerow(l)

    def writeQualComparison(self, *Quals):
        """"""
        qualList = list(Quals[0])
        org = qualList[0]
        sequenceNames = org.scoreCollectionHeaders
        with open(self.fileName, mode="w", newline='') as handle:
            writer = csv.writer(handle, delimiter='|', quoting=csv.QUOTE_NONE, dialect="excel", escapechar='\\')
            writer.writerow(['File created timestamp:', str(datetime.datetime.now())])
            l = ['File names:', 'Number of score collections in file:', 'Score collection lengths:', 'Average score:',
                 'Average score of file:', 'Total number of scores in file:']
            insert = ["" for i in range(0, len(sequenceNames)-1)]
            l[3:3] = insert
            l[3 + len(insert) + 1:3 + len(insert) +1] = insert
            writer.writerow(l)
            sequenceNames = sorted(sequenceNames)
            sequenceNames *= 2
            sequenceNames.insert(0, '')
            sequenceNames.insert(1, '')
            writer.writerow(sequenceNames)
            for x in qualList:
                if org.numberOfScoreCollections != x.numberOfScoreCollections:
                    for t in org.scoreCollectionLengths.keys():
                        if not t in x.scoreCollectionLengths.keys():
                            x.scoreCollectionLengths[t] = ""
                    for t in org.scoreCollectionAverage.keys():
                        if not t in x.scoreCollectionAverage.keys():
                            x.scoreCollectionAverage[t] = ""
            check = [True if sorted(org.scoreCollectionLengths.keys()) == sorted(x.scoreCollectionLengths.keys()) else
                     False for x in qualList]
            if set(check) == {True}:
                for x in qualList:
                    l = [x.name, x.numberOfScoreCollections]
                    l = l + [item[1]for item in sorted(x.scoreCollectionLengths.items())] + \
                        [item[1]for item in sorted(x.scoreCollectionAverage.items())]
                    l = l + [x.averageScoreFile] + [x.totalScoresCount]
                    writer.writerow(l)