"""
Name: AB1.py
Author: Ronald Nieuwenhuis
Date: 06-10-2015
Last update: 26-10-2015
Description:
"""


__author__ = 'ronald'

import os, sys, argparse, subprocess
from FileTypeError import FileTypeError
from Bio import SeqIO
from Bio.SeqIO.QualityIO import PairedFastaQualIterator
from subprocess import CalledProcessError

# File M24_S3-43_0397NSNP002F_P2.ab1 for instance cause a segmentation fault in tracetuner and aborts the whole
# pipeline..


class AB1:
    def __init__(self, filePath):
        """Initialize object, do checks."""
        self.fullPath = filePath
        pathSplit = filePath.split("/")
        self.fileName = pathSplit[-1]
        self.fileBaseName = self.fileName.split(".ab1")[0]
        self.basePathList = pathSplit[:-1]
        self.basePath = "/".join(pathSplit[:-1])
        if not os.path.isfile(self.fullPath):
            raise FileNotFoundError("File " + self.fullPath + " doesn't exist, is a directory or check file "
                                                              "permissions.")
        if not self.fileName.endswith(".ab1"):
            raise FileTypeError("File " + self.fullPath + " is not a .ab1 file.")

    def handFileToTraceTunerAndCreateFasta(self, outputDirName, winSize, threshold):
        """Execute a TraceTuner command with given outputdirectory, trim window size and average trim window threshold.
        If the command has been succesfully executed, the outputfile will have a lot of the noise, included at the start
        and end of the trace, less. TraceTuner does however not Trim the sequences literally, it does take out the
        leading and trailing rubbish, but the actual Trim is provided as information in the fasta description line. So
        the sequences have to be edited according this info. This is the second part of the function.
        """
        # TT does not send a returncode when a corrupted file is given!
        try:
            # Execute command.
            logName = outputDirName + "/" + self.fileBaseName + ".fasta.log"
            with open(logName, mode="w") as log:
                childprocess1 = subprocess.check_call(["ttuner", "-Q","-het","-trim_window", str(winSize),
                                                         "-trim_threshold", str(threshold), "-sd", outputDirName,
                                                         self.fullPath], stdout=log)
                try:
                    # TODO: For results gathering, 2 handles are opened at the same time and filenames differ. This has to be changed.

                    # Give the output a proper name, since TraceTuner fails to do this. Use linux 'mv' command.
                    oldOutput = outputDirName + "/" + self.fileName + ".seq"
                    newOutput = outputDirName + "/" + self.fileBaseName + ".fa"
                    trimmedOutput = outputDirName + "/" + self.fileBaseName + ".TRIMMED.fa"
                    childProcessTwo = subprocess.check_output(["mv", oldOutput, newOutput])

                    # Trim the sequences in the outputfile and write to a new file.
                    with open(newOutput, mode="r") as handle:
                        with open(trimmedOutput, mode="w") as handle2:
                            fileContent = handle.read()
                            fastas = fileContent.split(">")
                            for fasta in fastas[1:]:
                                lines = fasta.split("\n")
                                seq = ''
                                for seqPart in lines[1:len(lines)-1]:
                                    seq += seqPart
                                descriptionParts = lines[0].split(" ")
                                if len(descriptionParts) == 4:
                                    p = descriptionParts
                                    endSequence = seq[int(p[2]):int(p[3]) + int(p[2])]
                                else:
                                    raise Exception("No trimming information found.")
                                if len(endSequence) > 0:
                                    handle2.write(">" + lines[0] + " TRIMMED" + "\n")
                                    sequence = '\n'.join(endSequence[i:i+80] for i in range(0, len(endSequence), 80))
                                    handle2.write(sequence + "\n")
                except Exception as e:
                    raise Exception(e.args)
        except FileNotFoundError as e:
            raise FileNotFoundError("File not found.")
        except CalledProcessError as e:
            # HANDLING THIS ERROR MUST BE DONE BETTER BUT I HAVE NO IDEA HOW! The error itself is good but if you
            # process a lot of files it is good to know wich of the hundreds of files causes an error and why...
            raise CalledProcessError(e.returncode, e.cmd, e.output)

    def handFileToTraceTunerAndCreateQual(self, outputDirName, winSize, threshold):
        """Execute a TraceTuner command with given outputdirectory, trim window size and average trim window threshold.
        If the command has been succesfully executed, the outputfile will have a lot of the noise, included at the start
        and end of the trace, less. TraceTuner does however not Trim the sequences literally, it does take out the
        leading and trailing rubbish, but the actual Trim is provided as information in the fasta description line. So
        the scores have to be edited according this info. This is the second part of the function.
        """
        # TT does not send a returncode when a corrupted file is given.
        try:
            logName = outputDirName + "/" + self.fileBaseName + ".qual.log"
            with open(logName, mode="w") as log:
                childprocess1 = subprocess.check_call(["ttuner", "-Q","-het", "-trim_window", str(winSize),
                                                         "-trim_threshold",str(threshold), "-qd", outputDirName,
                                                         self.fullPath], stdout=log)
                try:
                    oldOutput = outputDirName + "/" + self.fileName + ".qual"
                    newOutput = outputDirName + "/" + self.fileBaseName + ".qual"
                    trimmedOutput = outputDirName + "/" + self.fileBaseName + ".TRIMMED.qual"
                    childProcessTwo = subprocess.check_output(["mv", oldOutput, newOutput ])
                    # Trim the quality values in the outputfile and write to a new file.
                    with open(newOutput, mode="r") as handle:
                        with open(trimmedOutput, mode = "w") as handle2:
                            fileContent = handle.read()
                            fastas = fileContent.split(">")
                            for fasta in fastas[1:]:
                                lines = fasta.split("\n")
                                allQuals = []
                                for qualPart in lines[1:len(lines)-1]:
                                    q = qualPart.split(" ")
                                    q = list(filter(lambda x: x != "", q))
                                    allQuals.extend(q)
                                p = lines[0].split(" ")
                                if len(p) == 4:
                                    endQuals = allQuals[int(p[2]):int(p[3]) + int(p[2])]
                                else:
                                    raise Exception("No trimming information found.")
                                if len(endQuals) > 0:
                                    endQuals = [" " + x if len(x) < 2 else x for x in endQuals[:]]
                                    endQuals = " ".join(endQuals[:])
                                    handle2.write(">" + lines[0] + "\n")
                                    gen = (endQuals[0+i:81+i] for i in range(0, len(endQuals), 81))
                                    for piece in gen:
                                        handle2.write(piece + "\n")
                except Exception as e:
                    raise Exception(e.args)
        except FileNotFoundError as e:
            raise FileNotFoundError("File not found.")
        except CalledProcessError as e:
            # HANDLING THIS ERROR MUST BE DONE BETTER BUT I HAVE NO IDEA HOW! The error itself is good but if you
            # process a lot of files it is good to know wich of the hundreds of files causes an error and why...
            raise CalledProcessError(e.returncode, e.cmd, e.output)

    def writeFastq(self, inputFasta, inputQual):
        """Create a fastq file out of a given fasta file and its associated qual file. No checks build in to check if
        files belong to eachother.
        """
        # TODO: Build in checks for the 2 files to see if they are associated.
        outputHandle = open(inputFasta.split(".TRIMMED.fa")[0] + ".fastq", mode = "w")
        records = PairedFastaQualIterator(open(inputFasta), open(inputQual))
        count = SeqIO.write(records, outputHandle, "fastq")
        outputHandle.close()

    def removeFastaAndQual(self, inputFasta, inputQual):
        """Remove given fasta and qual file."""
        # TODO: Build in try except clause in the light of EAFP.
        removeFasta= subprocess.call(["rm", inputFasta])
        removeQual = subprocess.call(["rm", inputQual])

    def __str__(self):
        return self.fullPath