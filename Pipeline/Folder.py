__author__ = 'ronald'

import os, sys, argparse, subprocess
from FileTypeError import FileTypeError
from NotAbsolutePathError import NotAbsolutePathError


class Folder:
    def __init__(self, folderName, input = True):
        """Initialize object, do checks."""
        # Is the abspath of any use??
        # self.path = os.path.abspath(folderName)
        self.path = folderName
        self.folderName = self.path.split("/")[-1]
        self.contents = []
        if not os.path.isabs(self.path):
            raise NotAbsolutePathError(str(self.path) + " is not an absolute path." )
        if not os.path.isdir(self.path):
            if not input:
                try:
                    self.checkOutputDir(self.path)
                except NotAbsolutePathError:
                    raise NotAbsolutePathError
            if input:
                raise NotADirectoryError(str(self.path) + " is not a directory.")

    def walkThroughDir(self, path = None):
        """Walk through the given directory and returns a list of the contents."""
        if path is None:
            path = self.path
        try:
            listDir = os.listdir(path)
        except PermissionError:
            raise PermissionError("Permission denied. Check directory permissions for directory: " + str(self.path))
        self.contents = [self.path + "/" + x for x in listDir]
        return self.contents

    def walkThroughTreeGetFilePaths(self, rootPath = None):
        """Go topdown recursively through the given directory and returns a list containing all the
        absolute file paths.
        """
        if rootPath is None:
            rootPath = self.path
        filePathList = [os.path.join(root, file) for root, dirs, files in os.walk(rootPath) for file in files]
        return filePathList

    def walkThroughTreeGetDirPaths(self, rootPath = None):
        """Go topdown recursively through the given directory and returns a list containing all the
        absolute directory paths.
        """

        if rootPath is None:
            rootPath = self.path
        dirPathList = [os.path.join(root, dirName) for root, dirs, files in os.walk(rootPath) for dirName in dirs]
        return dirPathList

    def checkOutputDir(self, destinationDirectory):
        """Check if the given directory exists, if not tries to make it exist."""

        if not os.path.abspath(destinationDirectory):
            raise NotAbsolutePathError(destinationDirectory)
        if not os.path.isdir(destinationDirectory):
            try:
                os.mkdir(destinationDirectory)
                print("Directory" + destinationDirectory + " created.")
            except FileNotFoundError:
                raise FileNotFoundError("The path to the targetdirectory is a not existing path.")
            except PermissionError:
                raise PermissionError("Permission denied. Check directory permissions")
            except FileExistsError:
                raise FileExistsError( str(destinationDirectory) + " is a file.")

    def __str__(self):
        return self.path