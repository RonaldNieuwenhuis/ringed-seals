__author__ = 'ronald'
import sys
from Folder import  Folder
import subprocess
import Bio.SeqIO
from subprocess import CalledProcessError
from Bio import SeqIO
from Bio.SeqIO.QualityIO import PairedFastaQualIterator


class Locus(Folder):
    def __init__(self, locusName, input=True):
        """Initialize object."""
        try:
            super().__init__(locusName, input)
            self.locusName = self.folderName
        except PermissionError:
            raise PermissionError("Permission denied. Check directory permissions")

    def handDirToTraceTunerAndCreateFasta(self, outputDirName, winSize, threshold):
        """Execute a TraceTuner command with given outputdirectory, trim window size and average trim window threshold.
        If the command has been succesfully executed, the outputfile will have a lot of the noise included at the start
        and end of the trace less. TraceTuner does however not Trim the sequences literally, it does take out the
        leading and trailing rubbish, but the actual Trim is provided as information in the fasta description line. So
        the sequence have to be edited according this info. This is the second part of the function.
        """

        # Create correct paths for outputfile.
        fileName = outputDirName + "/" + self.locusName + ".fa"
        fileNameTwo = outputDirName + "/" + self.locusName + ".TRIMMED.fa"  # Only for gathering results, name should stay as filename.
        # TT does not send a returncode when a corrupted file is given!
        try:
            logName = outputDirName + "/" + self.locusName + ".fasta.log"
            with open(logName, mode="w") as log:
                # Execute command.
                childprocess1 = subprocess.check_call(["ttuner", "-Q", "-het", "-trim_window", str(winSize),
                                                         "-trim_threshold", str(threshold), "-sa", fileName, "-id",
                                                         self.path], stdout=log, stderr=log)
                try:
                    # Trim the sequences in the outputfile and write to a new file.
                    # TODO: For results gathering, 2 handles are opened at the same time and filenames differ. This has to be changed.
                    with open(fileName, mode="r") as handle:
                        with open(fileNameTwo, mode="w") as handle2:
                            fileContent = handle.read()
                            fastas = fileContent.split(">")
                            for fasta in fastas[1:]:
                                lines = fasta.split("\n")
                                seq = ''
                                for seqPart in lines[1:]:
                                    seq += seqPart
                                descriptionParts = lines[0].split(" ")
                                if len(descriptionParts) == 4:
                                    p = descriptionParts
                                    endSequence = seq[int(p[2]):int(p[3]) + int(p[2])]
                                else:
                                    raise Exception("No trimming information found.")
                                if len(endSequence) > 0:
                                    handle2.write(">" + lines[0] + " TRIMMED" + "\n")
                                    sequence = '\n'.join(endSequence[i:i+80] for i in range(0, len(endSequence), 80))
                                    handle2.write(sequence + "\n")
                except Exception as e:
                    raise Exception(e.args)
        except FileNotFoundError as e:
            raise FileNotFoundError("File not found.")
        except CalledProcessError as e:
            # HANDLING THIS ERROR MUST BE DONE BETTER BUT I HAVE NO IDEA HOW! The error itself is good but if you
            # process a lot of files it is good to know wich of the hundreds of files causes an error and why...
            raise CalledProcessError(e.returncode, e.cmd, e.output)

    def handDirToTraceTunerAndCreateQual(self, outputDirName, winSize, threshold):
        """Execute a TraceTuner command with given outputdirectory, trim window size and average trim window threshold.
        If the command has been succesfully executed, the outputfile will have a lot of the noise included at the start
        and end of the trace less. TraceTuner does however not Trim the sequences literally, it does take out the
        leading and trailing rubbish, but the actual Trim is provided as information in the qual description line. So
        the quality scores have to be edited according this info. This is the second part of the function.
        """

        # Create correct paths for outputfile.
        x = self.path.split("/")    # Get the Locus folder name in order to give the files the right name.
        fileName = outputDirName + "/" + x[len(x)-1] + ".qual"
        fileNameTwo = outputDirName + "/" + x[len(x)-1] + ".TRIMMED.qual"
        # TT does not send a returncode when a corrupted file is given.
        try:
            logName = outputDirName + "/" + self.locusName + ".qual.log"
            with open(logName, mode="w") as log:
                # Execute command.
                childprocess1 = subprocess.check_call(["ttuner", "-Q", "-het", "-trim_window", str(winSize),
                                                         "-trim_threshold", str(threshold), "-qa", fileName, "-id",
                                                         self.path], stdout=log, stderr=log)
                try:
                    # Trim the quality values in the outputfile and write to a new file.
                    # TODO: For results gathering, 2 handles are opened at the same time and filenames differ. This has to be changed.
                    with open(fileName, mode="r") as handle:
                        with open(fileNameTwo, mode = "w") as handle2:
                            fileContent = handle.read()
                            fastas = fileContent.split(">")
                            fastas.remove('')
                            for fasta in fastas:
                                lines = fasta.split("\n")
                                allQuals = []
                                for qualPart in lines[1:len(lines)-1]:
                                    q = qualPart.split(" ")
                                    q = list(filter(lambda x: x != "", q))
                                    allQuals.extend(q)
                                p = lines[0].split(" ")
                                if len(p) == 4:
                                    endQuals = allQuals[int(p[2]):int(p[3]) + int(p[2])]
                                else:
                                    raise Exception("No trimming information found.")
                                if len(endQuals) > 0:
                                    endQuals = [" " + x if len(x) < 2 else x for x in endQuals[:]]
                                    endQuals = " ".join(endQuals[:])
                                    handle2.write(">" + lines[0] + "\n")
                                    gen = (endQuals[0+i:81+i] for i in range(0, len(endQuals), 81))
                                    for piece in gen:
                                        handle2.write(piece + "\n")
                except Exception as e:
                    raise Exception(e.args)
        except FileNotFoundError as e:
            raise FileNotFoundError("File not found.")
        except CalledProcessError as e:
            # HANDLING THIS ERROR MUST BE DONE BETTER BUT I HAVE NO IDEA HOW! The error itself is good but if you
            # process a lot of files it is good to know wich of the hundreds of files causes an error and why...
            raise CalledProcessError(e.returncode, e.cmd, e.output)

    def writeFastq(self,  inputFasta, inputQual):
        """Create a fastq file out of a given fasta file and its associated qual file. No checks build in to check if
        files belong to eachother.
        """
        # TODO: Build in checks for the 2 files to see if they are associated.
        outputHandle = open(inputFasta.split(".TRIMMED.fa")[0] + ".fastq", mode="w")
        records = PairedFastaQualIterator(open(inputFasta), open(inputQual))
        count = SeqIO.write(records, outputHandle, "fastq")
        outputHandle.close()

    def filterSequences(self, inputFasta, inputQual, outputFasta, outputQual, minScore):
        """"""
        fastq = inputFasta.split(".TRIMMED.fa")[0] + ".fastq"
        handle = open(fastq)
        parser = Bio.SeqIO.parse(handle, "fastq")
        averagePhredScoreBasket = []
        recordsList = list(parser)
        for record in recordsList:
            baseCounter = 0
            totalPhredCounter = 0
            for phred in record.letter_annotations["phred_quality"]:
                totalPhredCounter += int(phred)
                baseCounter += 1
            averagePhredScoreBasket.append([record.id, totalPhredCounter / baseCounter])
        handle.close()

        if not minScore:
            #OBSOLETE
            # totalOfAveragesCounter = 0
            # sequenceCounter = 0
            # for sequenceAverage in averagePhredScoreBasket:
            #     totalOfAveragesCounter += sequenceAverage[1]
            #     sequenceCounter += 1
            #
            # averageOfLocus = totalOfAveragesCounter/sequenceCounter
            minScore = sorted(averagePhredScoreBasket, reverse=True)[0] * 0.9
        outputHandle = open(fastq, mode="w")

        PhredsToKeep = []
        castAways = []
        for sequenceAndAverage in averagePhredScoreBasket:
            if sequenceAndAverage[1] < minScore:
                averagePhredScoreBasket.remove(sequenceAndAverage)
                castAways.append(sequenceAndAverage)
            elif sequenceAndAverage[1] >= minScore:
                PhredsToKeep.append(sequenceAndAverage)
            else:
                print("This should not happen.")

        for sequenceAndAverage in PhredsToKeep:
            for record in recordsList:
                if record.id == sequenceAndAverage[0]:
                    SeqIO.write(record, outputHandle, "fastq")
        outputHandle.close()

        handleTwo = open(inputFasta)
        handleThree = open(inputQual)

        fastaRead = handleTwo.read()
        qualRead = handleThree.read()

        fastaSplit = fastaRead.split(">")
        qualSplit = qualRead.split(">")

        handleTwo.close()
        handleThree.close()

        for sequenceAndAverage in castAways:
            for fasta in fastaSplit:
                if sequenceAndAverage[0] in fasta:
                    fastaSplit.remove(fasta)
            for qual in qualSplit:
                if sequenceAndAverage[0] in qual:
                    qualSplit.remove(qual)
        fastaResult = ">".join(fastaSplit)
        qualResult = ">".join(qualSplit)

        handleFour = open(outputFasta, mode="w")
        handleFive = open(outputQual, mode="w")

        handleFour.write(fastaResult)
        handleFive.write(qualResult)

        handleFour.close()
        handleFive.close()

    def removeFastaAndQual(self, inputFasta, inputQual):
        """Remove given fasta and qual file."""
        # TODO: Build in try except clause in the light of EAFP.
        removeFasta= subprocess.call(["rm", inputFasta])
        removeQual = subprocess.call(["rm", inputQual])

    def concatSeqFiles(self, filesList, initialLocusFasta):
        """Concatenate sequential files from fileslist and use filename as header."""
        with open(initialLocusFasta, mode="w") as handle:
            for file in filesList:
                with open(file) as seqFile:
                    content = seqFile.read()
                    handle.write(">" + file.split("/")[-1] + "\n")
                    handle.write(content)