import sys
import os.path
from EmptyFileError import EmptyFileError
from NotAbsolutePathError import NotAbsolutePathError


class Fasta:
    def __init__(self, filePath):
        """"Initialize object."""
        self.multi = False
        self._numberOfSequences = 0
        self._sequenceHeaders = []
        self._sequences = []
        self._records = {}
        self._sequenceLengths = {}
        self._sequenceCompositions = {}
        self._sequencePercentualCompositions = {}
        self._fileComposition = {}
        self._filePercentualComposition = {}
        self._totalNucleotideCount = 0
        self.fullPath = None
        self.basePath = None
        self.name = None
        self.basePathList = None
        try:
            os.path.isabs(filePath)
            self.fullPath = filePath
            pathSplit = filePath.split("/")
            self.name = pathSplit[-1]
            if self.name.endswith(".TRIMMED.fa"):
                self.baseName = pathSplit[-1].split(".TRIMMED.fa")[0]
            else:
                self.baseName = pathSplit[-1].split(".fa")[0]
            self.basePathList = pathSplit[:-1]
            self.basePath = "/".join(pathSplit[:-1])
        except NotAbsolutePathError:
            raise NotAbsolutePathError(str(filePath) + " is not an absolute path.")

    def setMulti(self, multi):
        """Set instance multi variable."""
        self.multi = bool(multi)

    @property
    def records(self):
        """The complete content of the Fasta file as dictionary with key = descriptor (alias headers, identifier, name,
        description) line and value = sequence.
        """
        return self._records

    @records.setter
    def records(self, records):
        if isinstance(records, dict):
            for record in records:
                if type(record) != str and type(records[record]) != str:
                    raise TypeError("Dictionary contains non string values.")
            self._records = records
        else:
            raise TypeError("Invalid argument type.")

    @property
    def numberOfSequences(self):
        """The number of sequences in the Fasta file counted according the number of '>' appearances."""
        return self._numberOfSequences

    @property
    def sequenceHeaders(self):
        """List with all of the descriptor (alias headers, identifier, name, description) of the Fasta file. """
        return self._sequenceHeaders

    @property
    def sequences(self):
        """List of all of the sequences of the Fasta file."""
        return self._sequences

    @property
    def sequenceLengths(self):
        """Dictionary of all the sequence lengths from the Fasta file. Key = descriptor, value = sequence length."""
        return self._sequenceLengths

    @property
    def sequenceCompositions(self):
        """Dictionary of all the sequence nucleotide compositions from the Fasta file. Key = descriptor, value =
        sequence nucleotide composition as a dicitonary on its own with key = nucleotide, value = nucleotide count.
        """
        return self._sequenceCompositions

    @property
    def sequencePercentualCompositions(self):
        """Dictionary of all of the percentual sequence nucleotide compositions from the Fasta file. Key = descriptor,
        value = percentual sequence nucleotide composition as a dictionary on its own with key = nucleotide,value =
        nucleotide count.
        """
        return self._sequencePercentualCompositions

    @property
    def fileComposition(self):
        """Dictionary of the complete nucleotide composition of the file. Key = nucleotide, value = nucleotide count."""
        return self._fileComposition

    @property
    def filePercentualComposition(self):
        """Dictionary of the complete nucleotide composition of the file. Key = nucleotide, value = nucleotide presence
        percentage.
        """
        return self._filePercentualComposition

    @property
    def totalNucleotideCount(self):
        """An integer representing the total count of nucleotides in the Fasta file."""
        return self._totalNucleotideCount

    def getSequencesCountFromFile(self):
        """Count sequences in file by occurance of descriptor symbol and set self.numberOfSequences."""
        try:
            with open(self.fullPath) as handle:
                content = handle.read()
                if len(content) == 0:
                    raise EmptyFileError("")
                numberOfFastas = content.count(">")
                if numberOfFastas == 0:
                    self._numberOfSequences = 1
                elif numberOfFastas == 1:
                    self._numberOfSequences = numberOfFastas
                else:
                    self._numberOfSequences = numberOfFastas
                    self.setMulti(True)
        except EmptyFileError:
            raise EmptyFileError("The given file is empty.")
        except FileNotFoundError:
            raise FileNotFoundError("File" + self.fullPath + " doesn't exist, is a directory or check file "
                                                             "permissions.")

    def getSequenceHeadersFromFile(self):
        """Get sequence descriptors (alias headers, identifier, name, description) from file."""
        try:
            with open(self.fullPath) as handle:
                content = handle.read()
                if len(content) == 0:
                    raise EmptyFileError("")
                if self.multi:
                    if content.startswith(">"):
                        fastas = content[1:].split(">")
                        for fasta in fastas:
                            fasta = fasta.split("\n")
                            self._sequenceHeaders.append(fasta[0])
                    else:
                        raise ValueError("If the file is a multifasta it needs '>' at the start of every descriptor "
                                         "line.")
                else:
                    fasta = content.split("\n")
                    firstLine = fasta[0].split(" ")[0].lstrip(">").split(".ab1")[0]
                    if firstLine == self.baseName:
                        self._sequenceHeaders.append(fasta[0].lstrip(">"))
                    else:
                        self._sequenceHeaders.append(self.name)
        except EmptyFileError:
            raise EmptyFileError("The given file is empty.")
        except FileNotFoundError:
            raise FileNotFoundError("File" + self.fullPath + " doesn't exist, is a directory or check file "
                                                             "permissions.")

    def getSequencesFromFile(self):
        """Get concatenated sequences from file."""
        try:
            with open(self.fullPath) as handle:
                content = handle.read()
                if len(content) == 0:
                    raise EmptyFileError("")
                if self.multi:
                    if content.startswith(">"):
                        fastas = content[1:].split(">")
                        for fasta in fastas:
                            fasta = fasta.split("\n")
                            self._sequences.append("".join(fasta[1:]).upper())
                    else:
                        raise ValueError("If the file is a multifasta it needs '>' at the start of every descriptor "
                                         "line.")
                else:
                    fasta = content.split("\n")
                    firstLine = fasta[0].split(" ")[0].lstrip(">").split(".ab1")[0]
                    if firstLine == self.baseName:
                        self._sequences.append("".join(fasta[1:]).upper())
                    else:
                        self._sequences.append("".join(fasta[:]).upper())
        except EmptyFileError:
            raise EmptyFileError("The given file is empty.")
        except FileNotFoundError:
            raise FileNotFoundError("File" + self.fullPath + " doesn't exist, is a directory or check file "
                                                             "permissions.")

    def getRecordsFromFile(self):
        """Get complete records in dictionary with the descriptor as key and sequence as value."""
        try:
            with open(self.fullPath) as handle:
                content = handle.read()
                if len(content) == 0:
                    raise EmptyFileError("")
                if self.multi:
                    if content.startswith(">"):
                        fastas = content[1:].split(">")
                        for fasta in fastas:
                            fasta = fasta.split("\n")
                            self._records[fasta[0].split(".")[0]] = "".join(fasta[1:]).upper()
                    else:
                        raise ValueError("If the file is a multifasta it needs '>' at the start of every descriptor "
                                         "line.")
                else:
                    fasta = content.split("\n")
                    firstLine = fasta[0].split(" ")[0].lstrip(">").split(".ab1")[0]
                    if firstLine == self.baseName:
                        self._records[fasta[0].lstrip(">").split(".")[0]] = "".join(fasta[1:]).upper()
                    else:
                        self._records[self.name.split(".")[0]] = "".join(fasta[:]).upper()
        except EmptyFileError:
            raise EmptyFileError("The given file is empty.")
        except FileNotFoundError:
            raise FileNotFoundError("File" + self.fullPath + " doesn't exist, is a directory or check file "
                                                             "permissions.")

    def getSequencesLengthsFromFile(self):
        """Get length of sequences in file."""
        try:
            with open(self.fullPath) as handle:
                content = handle.read()
                if len(content) == 0:
                    raise EmptyFileError("")
                if self.multi:
                    if content.startswith(">"):
                        fastas = content[1:].split(">")
                        for fasta in fastas:
                            fasta = fasta.split("\n")
                            self._sequenceLengths[fasta[0].split(".")[0]] = len("".join(fasta[1:]))
                    else:
                        raise ValueError("If the file is a multifasta it needs '>' at the start of every descriptor "
                                         "line.")
                else:
                    fasta = content.split("\n")
                    firstLine = fasta[0].split(" ")[0].lstrip(">").split(".ab1")[0]
                    if firstLine == self.baseName:
                        self._sequenceLengths[fasta[0].lstrip(">").split(".")[0]] = len("".join(fasta[1:]))
                    else:
                        self._sequenceLengths[self.name.split(".")[0]] = len("".join(fasta[:]))
        except EmptyFileError:
            raise EmptyFileError("The given file is empty.")
        except FileNotFoundError:
            raise FileNotFoundError("File" + self.fullPath + " doesn't exist, is a directory or check file "
                                                             "permissions.")

    def getSequenceCompositionFromFile(self):
        """Get the nucleotide composition of each sequence in absolute and percentual number and the cummulative
        numbers of those from the Fasta.
        """
        try:
            with open(self.fullPath) as handle:
                content = handle.read()
                if len(content) == 0:
                    raise EmptyFileError("")
                if self.multi:
                    if content.startswith(">"):
                        fastas = content[1:].split(">")
                        for fasta in fastas:
                            compDict = {}
                            fasta = fasta.split("\n")
                            for seq in fasta[1:]:
                                for nuc in seq:
                                    nuc = nuc.upper()
                                    compDict[nuc] = compDict.get(nuc, 0) + 1
                                    self._fileComposition[nuc] = self._fileComposition.get(nuc, 0) + 1
                            self._sequenceCompositions[fasta[0].split(".")[0]] = compDict
                            seqLength = 0
                            for key, value in compDict.items():
                                seqLength += value
                            self._sequencePercentualCompositions[fasta[0].split(".")[0]] = {key: round(value/seqLength*100, 2) for
                                                                              key, value in compDict.items()}
                    else:
                        raise ValueError("If the file is a multifasta it needs '>' at the start of every descriptor "
                                         "line.")
                else:
                    fasta = content.split("\n")
                    firstLine = fasta[0].split(" ")[0].lstrip(">").split(".ab1")[0]
                    if firstLine == self.baseName:
                        compDict = {}
                        for seq in fasta[1:]:
                            for nuc in seq:
                                nuc = nuc.upper()
                                compDict[nuc] = compDict.get(nuc, 0) + 1
                                self._fileComposition[nuc] = self._fileComposition.get(nuc, 0) + 1
                        self._sequenceCompositions[firstLine.split(".")[0]] = compDict
                        seqLength = 0
                        for key, value in compDict.items():
                            seqLength += value
                        self._sequencePercentualCompositions[firstLine.split(".")[0]] = {key: round(value/seqLength*100, 2) for key,
                                                                           value in compDict.items()}
                    else:
                        compDict = {}
                        for seq in fasta[:]:
                            for nuc in seq:
                                nuc = nuc.upper()
                                compDict[nuc] = compDict.get(nuc, 0) + 1
                                self._fileComposition[nuc] = self._fileComposition.get(nuc, 0) + 1
                        self._sequenceCompositions[self.name.split(".")[0]] = compDict
                        seqLength = 0
                        for key, value in compDict.items():
                            seqLength += value
                        self._sequencePercentualCompositions[self.name.split(".")[0]] = {key: round(value/seqLength*100, 2)
                                                                           for key, value in compDict.items()}
                for key, value in self._fileComposition.items():
                    self._totalNucleotideCount += value
                self._filePercentualComposition = {key: round(value/self._totalNucleotideCount*100, 2) for key, value in
                                                   self._fileComposition.items()}
        except EmptyFileError:
            raise EmptyFileError("The given file is empty.")
        except FileNotFoundError:
            raise FileNotFoundError("File" + self.fullPath + " doesn't exist, is a directory or check file "
                                                             "permissions.")

    def getSequencesCountFromRecords(self):
        """Get the total number of sequences from the self._records attribute and set self_numberOfSequences."""
        if len(self._records) > 0:
            self._numberOfSequences = len(self._records)
            if len(self._records) > 1:
                self.setMulti(True)
        else:
            raise EmptyFileError("Self.records is empty.")

    def getSequenceHeadersFromRecords(self):
        """Get sequence descriptors (alias headers, identifier, name, description) from self._records. """
        if len(self._records) > 0:
            for record in self._records:
                self._sequenceHeaders.append(record)
        else:
            raise EmptyFileError("Self.records is empty.")

    def getSequencesFromRecords(self):
        """Get sequences from self._records. """
        if len(self._records) > 0:
            for record in self._records:
                self._sequences.append(self._records[record])
        else:
            raise EmptyFileError("Self.records is empty.")

    def getSequencesLengthsFromRecord(self):
        """Get sequences lengths from self._records and put them in a dictionary.self._sequenceLength with key =
        descriptor, value = sequence length.
        """
        if len(self._records) > 0:
            for key, value in self._records.items():
                self._sequenceLengths[key] = len(value)
        else:
            raise EmptyFileError("Self.records is empty.")

    def getSequenceCompositionFromRecord(self):
        """Get the nucleotide composition of each sequence in absolute and percentual number and the cummulative
        numbers of those from self._records.
        """
        if len(self._records) > 0:
            for key, value in self._records.items():
                compDict = {}
                for x in value:
                    compDict[x] = compDict.get(x, 0) + 1
                percCompDict = {x.upper(): round(compDict.get(x.upper(), 0)/len(value)*100, 2) for x in compDict}
                self._sequenceCompositions[key] = compDict
                self._sequencePercentualCompositions[key] = percCompDict
                for x in value:
                    x = x.upper()
                    self._fileComposition[x] = self._fileComposition.get(x, 0) + 1
            for key, value in self._fileComposition.items():
                self._totalNucleotideCount += value
            self._filePercentualComposition = {key: round(value/self._totalNucleotideCount*100, 2) for key, value in
                                               self._fileComposition.items()}
        else:
            raise EmptyFileError("Self.records is empty.")

    def doAllFromFile(self):
        """Get all properties from File."""
        try:
            self.getSequencesCountFromFile()
            self.getRecordsFromFile()
            self.getSequenceHeadersFromFile()
            self.getSequencesFromFile()
            self.getSequencesLengthsFromFile()
            self.getSequenceCompositionFromFile()
        except EmptyFileError as e:
            raise EmptyFileError(e.msg)
        except FileNotFoundError as e:
            raise FileNotFoundError(e.args)

    def doAllFromRecords(self):
        """Get all properties from records."""
        try:
            self.getSequencesCountFromRecords()
            self.getSequenceHeadersFromRecords()
            self.getSequencesFromRecords()
            self.getSequencesLengthsFromRecord()
            self.getSequenceCompositionFromRecord()
        except EmptyFileError as e:
            raise EmptyFileError(e.msg)

    def writeFasta(self):
        """Write a fasta file from the records."""
        try:
            with open(self.fullPath, mode="w") as handle:
                for header, sequence in self._records.items():
                    sequence = '\n'.join(sequence[i:i+80] for i in range(0, len(sequence), 80))
                    handle.write(">" + header + "\n" + sequence + "\n")
        except PermissionError:
            raise PermissionError("You do not have write permissions.")

    def __str__(self):
        """"""
        s = "Fasta filename:  {}  \n".format(self.name)
        return s