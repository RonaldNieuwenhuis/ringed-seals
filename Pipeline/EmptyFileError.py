class EmptyFileError(BaseException):
    def __init__(self, msg):
        self.msg = msg
        BaseException.__init__(self, msg)