import os
from NotAbsolutePathError import NotAbsolutePathError
from EmptyFileError import EmptyFileError


class Qual:
    def __init__(self, filePath):
        self.multi = False
        self._numberOfScoreCollections = 0
        self._scoreCollectionHeaders = []
        self._scoreCollections = []
        self._records = {}
        self._scoreCollectionLengths = {}
        self._scoreCollectionAverage = {}
        self._averageScoreFile = 0
        self._totalScoresCount = 0
        self.fullPath = None
        self.basePath = None
        self.name = None
        self.basePathList = None
        try:
            os.path.isabs(filePath)
            self.fullPath = filePath
            pathSplit = filePath.split("/")
            self.name = pathSplit[-1]
            if self.name.endswith(".TRIMMED.qual"):
                self.baseName = pathSplit[-1].split(".TRIMMED.qual")[0]
            else:
                self.baseName = pathSplit[-1].split(".qual")[0]
            self.basePathList = pathSplit[:-1]
            self.basePath = "/".join(pathSplit[:-1])
        except NotAbsolutePathError:
            raise NotAbsolutePathError(str(filePath) + " is not an absolute path.")

    def setMulti(self, multi):
        """Set instance multi variable."""
        self.multi = bool(multi)

    @property
    def records(self):
        """The complete content of the Fasta file as dictionary with key = descriptor (alias headers, identifier, name,
        description) line and value = sequence.
        """
        return self._records

    @records.setter
    def records(self, records):
        if isinstance(records, dict):
            for record in records:
                if type(record) != str and type(records[record]) != str:
                    raise TypeError("Dictionary contains non string values.")
            self._records = records
        else:
            raise TypeError("Invalid argument type.")

    @property
    def numberOfScoreCollections(self):
        """The number of scores in the Fasta file counted according the number of '>' appearances."""
        return self._numberOfScoreCollections

    @property
    def scoreCollectionHeaders(self):
        """List with all of the descriptor (alias headers, identifier, name, description) of the Fasta file. """
        return self._scoreCollectionHeaders

    @property
    def scoreCollections(self):
        """List of all of the scores of the Fasta file."""
        return self._scoreCollections

    @property
    def scoreCollectionLengths(self):
        """Dictionary of all the score lengths from the Fasta file. Key = descriptor, value = score length."""
        return self._scoreCollectionLengths

    @property
    def scoreCollectionAverage(self):
        """Dictionary of the complete nucleotide composition of the file. Key = nucleotide, value = nucleotide count."""
        return self._scoreCollectionAverage

    @property
    def averageScoreFile(self):
        """The average base score of the complete file."""
        return self._averageScoreFile

    @property
    def totalScoresCount(self):
        """Total number of phred scores in the File."""
        return self._totalScoresCount

    def getScoreCollectionHeadersFromFile(self):
        """Get score descriptors (alias headers, identifier, name, description) from file."""
        try:
            with open(self.fullPath) as handle:
                content = handle.read()
                if len(content) == 0:
                    raise EmptyFileError("")
                if self.multi:
                    if content.startswith(">"):
                        collections = content[1:].split(">")
                        for collection in collections:
                            collection = collection.split("\n")
                            self._scoreCollectionHeaders.append(collection[0])
                    else:
                        raise ValueError("If the file is a multifasta it needs '>' at the start of every descriptor "
                                         "line.")
                else:
                    collection = content.split("\n")
                    firstLine = collection[0].split(" ")[0].lstrip(">").split(".ab1")[0]
                    if firstLine == self.baseName:
                        self._scoreCollectionHeaders.append(collection[0].lstrip(">"))
                    else:
                        self._scoreCollectionHeaders.append(self.name)
        except EmptyFileError:
            raise EmptyFileError("The given file is empty.")
        except FileNotFoundError:
            raise FileNotFoundError("File" + self.fullPath + " doesn't exist, is a directory or check file "
                                                             "permissions.")

    def getRecordsFromFile(self):
        """Get complete records in dictionary with the descriptor as key and score collection as value."""
        try:
            with open(self.fullPath) as handle:
                content = handle.read()
                if len(content) == 0:
                    raise EmptyFileError("")
                if self.multi:
                    if content.startswith(">"):
                        collections = content[1:].split(">")
                        for collection in collections:
                            collection = collection.split("\n")
                            self._records[collection[0].split(".")[0]] = "".join(collection[1:])
                    else:
                        raise ValueError("If the file is a multifasta it needs '>' at the start of every descriptor "
                                         "line.")
                else:
                    collection = content.split("\n")
                    firstLine = collection[0].split(" ")[0].lstrip(">").split(".ab1")[0]
                    if firstLine == self.baseName:
                        self._records[collection[0].lstrip(">").split(".")[0]] = "".join(collection[1:])
                    else:
                        self._records[self.name.split(".")[0]] = "".join(collection[:])
        except EmptyFileError:
            raise EmptyFileError("The given file is empty.")
        except FileNotFoundError:
            raise FileNotFoundError("File" + self.fullPath + " doesn't exist, is a directory or check file "
                                                             "permissions.")

    def getScoreCollectionsFromFile(self):
        """Get concatenated score collections from file."""
        try:
            with open(self.fullPath) as handle:
                content = handle.read()
                if len(content) == 0:
                    raise EmptyFileError("")
                if self.multi:
                    if content.startswith(">"):
                        collections = content[1:].split(">")
                        for collection in collections:
                            collection = collection.split("\n")
                            self._scoreCollections.append("".join(collection[1:]))
                    else:
                        raise ValueError("If the file is a multifasta it needs '>' at the start of every descriptor "
                                         "line.")
                else:
                    collection = content.split("\n")
                    firstLine = collection[0].split(" ")[0].lstrip(">").split(".ab1")[0]
                    if firstLine == self.baseName:
                        self._scoreCollections.append("".join(collection[1:]))
                    else:
                        self._scoreCollections.append("".join(collection[:]))
        except EmptyFileError:
            raise EmptyFileError("The given file is empty.")
        except FileNotFoundError:
            raise FileNotFoundError("File" + self.fullPath + " doesn't exist, is a directory or check file "
                                                             "permissions.")

    def getScoreCollectionLengthsFromFile(self):
        """Get length of score collections in file."""
        try:
            with open(self.fullPath) as handle:
                content = handle.read()
                if len(content) == 0:
                    raise EmptyFileError("")
                if self.multi:
                    if content.startswith(">"):
                        collections = content[1:].split(">")
                        for collection in collections:
                            collection = collection.split("\n")
                            self._scoreCollectionLengths[collection[0].split(".")[0]] = len("".join(collection[1:]).split())
                    else:
                        raise ValueError("If the file is a multifasta it needs '>' at the start of every descriptor "
                                         "line.")
                else:
                    collection = content.split("\n")
                    firstLine = collection[0].split(" ")[0].lstrip(">").split(".ab1")[0]
                    if firstLine == self.baseName:
                        self._scoreCollectionLengths[collection[0].lstrip(">").split(".")[0]] = len("".join(collection[1:]).split())
                    else:
                        self._scoreCollectionLengths[self.name.split(".")[0]] = len("".join(collection[:]).split())
        except EmptyFileError:
            raise EmptyFileError("The given file is empty.")
        except FileNotFoundError:
            raise FileNotFoundError("File" + self.fullPath + " doesn't exist, is a directory or check file "
                                                             "permissions.")

    def getNumberOfScoreCollectionsFromFile(self):
        """Count score collections in file by occurance of descriptor symbol '>' and set
        self.numberOfScoreCollections.
        """
        try:
            with open(self.fullPath) as handle:
                content = handle.read()
                if len(content) == 0:
                    raise EmptyFileError("")
                numberOfScoreCollections = content.count(">")
                if numberOfScoreCollections == 0:
                    self._numberOfScoreCollections = 1
                elif numberOfScoreCollections == 1:
                    self._numberOfScoreCollections = numberOfScoreCollections
                else:
                    self._numberOfScoreCollections = numberOfScoreCollections
                    self.setMulti(True)
        except EmptyFileError:
            raise EmptyFileError("The given file is empty.")
        except FileNotFoundError:
            raise FileNotFoundError("File" + self.fullPath + " doesn't exist, is a directory or check file "
                                                             "permissions.")

    def getScoreCollectionAverageFromFile(self):
        """Get the average score of each collection in absolute and the average score of the complete file."""
        try:
            with open(self.fullPath) as handle:
                content = handle.read()
                totalValue = 0
                if len(content) == 0:
                    raise EmptyFileError("")
                if self.multi:
                    if content.startswith(">"):
                        collections = content[1:].split(">")
                        for collection in collections:
                            collection = collection.split("\n")
                            scores = "".join(collection[1:])
                            collectionTotalValue = sum([int(i) for i in scores.split()])
                            collectionLength = len(scores.split())
                            self._totalScoresCount += collectionLength
                            totalValue += collectionTotalValue
                            self._scoreCollectionAverage[collection[0].split(".")[0]] = round(collectionTotalValue /
                                                                                collectionLength, 1)
                    else:
                        raise ValueError("If the file is a multifasta it needs '>' at the start of every descriptor "
                                         "line.")
                else:
                    collection = content.split("\n")
                    firstLine = collection[0].split(" ")[0].lstrip(">").split(".ab1")[0]
                    if firstLine == self.baseName:
                        scores = "".join(collection[1:])
                        collectionTotalValue = sum([int(i) for i in scores.split()])
                        collectionLength = len(scores.split())
                        self._totalScoresCount += collectionLength
                        totalValue += collectionTotalValue
                        self._scoreCollectionAverage[firstLine.split(".")[0]] = round(collectionTotalValue / collectionLength, 1)
                    else:
                        scores = "".join(collection[:])
                        collectionTotalValue = sum([int(i) for i in scores.split()])
                        collectionLength = len(scores.split())
                        self._totalScoresCount += collectionLength
                        totalValue += collectionTotalValue
                        self._scoreCollectionAverage[firstLine.split(".")[0]] = round(collectionTotalValue / collectionLength, 1)
                self._averageScoreFile = round(totalValue / self._totalScoresCount, 1)
        except EmptyFileError:
            raise EmptyFileError("The given file is empty.")
        except FileNotFoundError:
            raise FileNotFoundError("File" + self.fullPath + " doesn't exist, is a directory or check file "
                                                             "permissions.")

    def getScoreCollectionHeadersFromRecords(self):
        """Get score descriptors (alias headers, identifier, name, description) from self._records. """
        if len(self._records) > 0:
            for record in self._records:
                self._scoreCollectionHeaders.append(record)
        else:
            raise EmptyFileError("Self.records is empty.")

    def getScoreCollectionsFromRecords(self):
        """Get score collections from self._records. """
        if len(self._records) > 0:
            for record in self._records:
                self._scoreCollections.append(self._records[record])
        else:
            raise EmptyFileError("Self.records is empty.")

    def getScoreCollectionLengthsFromRecords(self):
        """Get score collection lengths from self._records and put them in a dictionary.self._sequenceLength with key =
        descriptor, value = score collection length.
        """
        if len(self._records) > 0:
            for key, value in self._records.items():
                self._scoreCollectionLengths[key] = len(value.split())
        else:
            raise EmptyFileError("Self.records is empty.")

    def getNumberOfScoreCollectionsFromRecords(self):
        """Get the total number of score collections from the self._records attribute and set
        self._numberOfScoreCollections.
        """
        if len(self._records) > 0:
            self._numberOfScoreCollections = len(self._records)
            if len(self._records) > 1:
                self.setMulti(True)
        else:
            raise EmptyFileError("Self.records is empty.")

    def getScoreCollectionAverageFromRecords(self):
        """Get the average score of each collection and the average score of the file from self._records."""
        if len(self._records) > 0:
            totalValue = 0
            for key, value in self._records.items():
                collectiontotalValue = sum(value.split())
                collectionLength = len(value.split())
                totalValue += collectiontotalValue
                self._totalScoresCount += collectionLength
                self._scoreCollectionAverage[key] = round(collectiontotalValue / collectionLength, 1)
            self._averageScoreFile = round(totalValue / self._totalScoresCount, 1)
        else:
            raise EmptyFileError("Self.records is empty.")

    def doAllFromFile(self):
        """Get all properties from File."""
        try:
            self.getNumberOfScoreCollectionsFromFile()
            self.getRecordsFromFile()
            self.getScoreCollectionsFromFile()
            self.getScoreCollectionAverageFromFile()
            self.getScoreCollectionHeadersFromFile()
            self.getScoreCollectionLengthsFromFile()
        except EmptyFileError as e:
            raise EmptyFileError(e.msg)
        except FileNotFoundError as e:
            raise FileNotFoundError(e.args)

    def doAllFromRecords(self):
        """Get all properties from records."""
        try:
            self.getNumberOfScoreCollectionsFromRecords()
            self.getScoreCollectionHeadersFromRecords()
            self.getScoreCollectionLengthsFromRecords()
            self.getScoreCollectionsFromRecords()
            self.getScoreCollectionAverageFromRecords()
        except EmptyFileError as e:
            raise EmptyFileError(e.msg)

    def writeQual(self):
        """Write a qual file from the records."""
        try:
            with open(self.fullPath, mode="w") as handle:
                for header, collection in self._records.items():
                    handle.write(">" + header + "\n")
                    gen = (collection[0+i:81+i] for i in range(0, len(collection), 81))
                    for piece in gen:
                        handle.write(piece + "\n")
        except PermissionError:
            raise PermissionError("You do not have write permission.")

    def __str__(self):
        """"""
        s = "Qual filename:  {}\n".format(self.name)
        return s
