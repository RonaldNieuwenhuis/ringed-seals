#!/usr/bin/python3.4
"""
Name: Pipeline.py
Author: Ronald Nieuwenhuis
Date: 06-10-2015
Last update:
Description:
"""
import os
import sys
import argparse
import shutil
import ParseSummaries
import subprocess
from FileTypeError import FileTypeError
from NotAbsolutePathError import NotAbsolutePathError
from subprocess import CalledProcessError
from Folder import Folder
from Locus import Locus
from AB1 import AB1
from Fasta import Fasta
from Qual import Qual
from Overview import Overview


__author__ = 'ronald'


def main():
    # Loads of stuff for the commandline parsing.
    parser = argparse.ArgumentParser(prog='Pipeline.py', description=__doc__, formatter_class=argparse.HelpFormatter)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-i', '--inputfile', help='Input file name')
    group.add_argument('-d', '--inputdir', help='Inputdirectory with input files')
    parser.add_argument('-a', help='Look recursively for inputfiles in subdirectories in -d/--inputdir',
                        action='store_true', required=False, dest='recur', default=False)
    parser.add_argument('-t', '--targetdir', help='Destination directory for results', default=str(os.getcwd()),
                        required=False, nargs='?', const=str(os.getcwd()))
    parser.add_argument('-w', '--windowSize', help='Window size for the trimming window', default=[10],
                        required=False, nargs=1)
    parser.add_argument('-q', '--trimQuality', help='threshold of the average quality in the trimming window',
                        default=[20], required=False, nargs=1, type=int)
    parser.add_argument('-o', '--outputType', required=True, help='The type of outputfile that should be created.',
                        nargs=1, type=str, choices=['fasta', 'fastq', 'qual', 'all'])
    parser.add_argument('-s', '--scope', required=True, help='The scope the pipeline works on. This can be for inst'
                        'ance per  trace or per locus.', nargs=1, type=str,
                        choices=['trace', 'locus'])
    parser.add_argument('-ft', '--filterThreshold', help='threshold value of the filter for low average quality '
                                                         'sequences', required=False, nargs=1, type=int, default=[0])
    parser.add_argument('-os', '--outputSummary', help='Generate a comparison of pre and post called traces.',
                        action='store_true', required=False, dest='summary', default=False)
    parser.add_argument('-st', '--Statistics', help='Return the pipeline effectiveness statistics in the form of visual'
                                                    'plots.', action='store_true', required=False, dest='stats',
                        default=False)
    args = parser.parse_args()

    # Start the process.

    # Check if ttuner executable is in path.
    if not shutil.which("ttuner"):
        usage("Executable ttuner has to be in $PATH.", True)

    # Make Folder object of the outputdir to put automatically some checks on it.
    # If no outputdir is provided, current working directory is used.
    outputDir = str(args.targetdir)

    # Also make an overview object which makes a summary of the results of the pipeline. Good to show effectiveness of
    # the pipeline.
    # overview = Overview()

    try:
        outputDir = Folder(outputDir, False)
    except (NotADirectoryError, NotAbsolutePathError, PermissionError, FileExistsError, FileNotFoundError) as e:
        usage(e, True)
    windowSize = int(args.windowSize[0])
    threshold = int(args.trimQuality[0])
    outputType = str(args.outputType[0])
    scope = str(args.scope[0])
    minScore = float(args.filterThreshold[0])

    # Scenario 1: user provides 1 inputfile.
    if args.inputfile:
        fileName = str(args.inputfile)
        try:
            # The inputfile is an AB1 file. Make an instance of it.
            a = AB1(fileName)
            oldFasta = fileName.split(".ab1")[0] + ".seq"  # Seq hardcoded suffix.
            b = Fasta(oldFasta)
            b.doAllFromFile()
            if outputType == 'fasta':
                a.handFileToTraceTunerAndCreateFasta(str(outputDir), windowSize, threshold)
                if args.summary:
                    newFileName = str(outputDir) + "/" + a.fileBaseName + ".TRIMMED.fa"
                    beforeTrim = str(outputDir) + "/" + a.fileBaseName + ".fa"
                    summaryName = str(outputDir) + "/" + a.fileBaseName + ".FASTASUMMARY" + ".csv"
                    c = Fasta(beforeTrim)
                    c.doAllFromFile()
                    d = Fasta(newFileName)
                    d.doAllFromFile()
                    h = Overview(summaryName, b, c, d)
            elif outputType == 'qual':
                a.handFileToTraceTunerAndCreateQual(str(outputDir), windowSize, threshold)
                if args.summary:
                    newFileName = str(outputDir) + "/" + a.fileBaseName + ".TRIMMED.qual"
                    beforeTrim = str(outputDir) + "/" + a.fileBaseName + ".qual"
                    summaryName = str(outputDir) + "/" + a.fileBaseName + ".QUALSUMMARY" + ".csv"
                    c = Qual(beforeTrim)
                    c.doAllFromFile()
                    d = Qual(newFileName)
                    d.doAllFromFile()
                    h = Overview(summaryName, c, d)
            elif outputType == 'fastq' or outputType == 'all':
                # Both a fasta and qual file have to be made first in order to make a fastq file.
                a.handFileToTraceTunerAndCreateFasta(str(outputDir), windowSize, threshold)
                a.handFileToTraceTunerAndCreateQual(str(outputDir), windowSize, threshold)
                newFastaFileName = str(outputDir) + "/" + a.fileBaseName + ".TRIMMED.fa"
                fastaBeforeTrim = str(outputDir) + "/" + a.fileBaseName + ".fa"
                newQualFileName = str(outputDir) + "/" + a.fileBaseName + ".TRIMMED.qual"
                qualBeforeTrim = str(outputDir) + "/" + a.fileBaseName + ".qual"
                if args.summary:
                    fastaSummaryName = str(outputDir) + "/" + a.fileBaseName + ".FASTASUMMARY" + ".csv"
                    qualSummaryName = str(outputDir) + "/" + a.fileBaseName + ".QUALSUMMARY" + ".csv"
                    c1 = Fasta(fastaBeforeTrim)
                    c1.doAllFromFile()
                    c2 = Qual(qualBeforeTrim)
                    c2.doAllFromFile()
                    d1 = Fasta(newFastaFileName)
                    d1.doAllFromFile()
                    d2 = Qual(newQualFileName)
                    d2.doAllFromFile()
                    g = Overview(fastaSummaryName, b, c1, d1)
                    h = Overview(qualSummaryName, c2, d2)
                a.writeFastq(newFastaFileName, newQualFileName)
                if not outputType == 'all':
                    a.removeFastaAndQual(newFastaFileName, newQualFileName)
                    a.removeFastaAndQual(qualBeforeTrim, fastaBeforeTrim)
                    # Catch exceptions.
        except (FileNotFoundError, FileTypeError) as e:
            usage(e, True)

    # Scenario 2: User provides a directory with multiple input files.
    if args.inputdir:
        topDirName = str(args.inputdir)
        try:
            folder = Folder(topDirName, True)
        except (NotADirectoryError, NotAbsolutePathError, PermissionError, FileExistsError) as e:
            usage(e, True)
        # Scenario 2A: User wants the pipeline to go recursively through subdirectories and process all files that
        # can be used (greedy).
        if args.recur:
            filePaths = folder.walkThroughTreeGetFilePaths()
            dirPaths = folder.walkThroughTreeGetDirPaths()
            targetDirPathTuples = [(str(outputDir) + str(dirPath.split(topDirName)[len(dirPath.split(topDirName))-1]),
                                    dirPath) for dirPath in dirPaths]
            filePathTuples = [(filePath, str(outputDir) + "/" + "/".join(filePath.split("/")
                                                                         [len(topDirName.split("/")):
                                                                         len(filePath.split("/"))-1])) for filePath in
                              filePaths]
            for dirName in targetDirPathTuples:
                try:
                    Folder(dirName[0], False)
                except NotAbsolutePathError as e:
                    usage(e, False)
                except PermissionError as e:
                    usage(e, True)
            if scope == "trace":
                for files in filePathTuples:
                    try:
                        # The inputfile is an AB1 file.
                        a = AB1(files[0])
                        oldFasta = files[0].split(".ab1")[0] + ".seq"  # Seq hardcoded suffix.
                        b = Fasta(oldFasta)
                        b.doAllFromFile()
                        if outputType == 'fasta':
                            a.handFileToTraceTunerAndCreateFasta(files[1], windowSize, threshold)
                            if args.summary:
                                newFileName = str(outputDir) + "/" + a.fileBaseName + ".TRIMMED.fa"
                                beforeTrim = str(outputDir) + "/" + a.fileBaseName + ".fa"
                                summaryName = str(outputDir) + "/" + a.fileBaseName + ".FASTASUMMARY" + ".csv"
                                c = Fasta(beforeTrim)
                                c.doAllFromFile()
                                d = Fasta(newFileName)
                                d.doAllFromFile()
                                g = Overview(summaryName, b, c, d)
                        elif outputType == 'qual':
                            a.handFileToTraceTunerAndCreateQual(files[1], windowSize, threshold)
                            if args.summary:
                                newFileName = str(outputDir) + "/" + a.fileBaseName + ".TRIMMED.qual"
                                beforeTrim = str(outputDir) + "/" + a.fileBaseName + ".qual"
                                summaryName = str(outputDir) + "/" + a.fileBaseName + ".QUALSUMMARY" + ".csv"
                                c = Qual(beforeTrim)
                                c.doAllFromFile()
                                d = Qual(newFileName)
                                d.doAllFromFile()
                                g = Overview(summaryName, c, d)
                        elif outputType == 'fastq' or outputType == 'all':
                            # Both a fasta and qual file have to be made first in order to make a fastq file.
                            a.handFileToTraceTunerAndCreateFasta(files[1], windowSize, threshold)
                            a.handFileToTraceTunerAndCreateQual(files[1], windowSize, threshold)
                            newFastaFileName = str(outputDir) + "/" + a.fileBaseName + ".TRIMMED.fa"
                            fastaBeforeTrim = str(outputDir) + "/" + a.fileBaseName + ".fa"
                            newQualFileName = str(outputDir) + "/" + a.fileBaseName + ".TRIMMED.qual"
                            qualBeforeTrim = str(outputDir) + "/" + a.fileBaseName + ".qual"
                            if args.summary:
                                fastaSummaryName = str(outputDir) + "/" + a.fileBaseName + ".FASTASUMMARY" + ".csv"
                                qualSummaryName = str(outputDir) + "/" + a.fileBaseName + ".QUALSUMMARY" + ".csv"
                                c1 = Fasta(fastaBeforeTrim)
                                c1.doAllFromFile()
                                c2 = Qual(qualBeforeTrim)
                                c2.doAllFromFile()
                                d1 = Fasta(newFastaFileName)
                                d1.doAllFromFile()
                                d2 = Qual(newQualFileName)
                                d2.doAllFromFile()
                                g = Overview(fastaSummaryName, b, c1, d1)
                                h= Overview(qualSummaryName, c2, d2)
                            a.writeFastq(newFastaFileName, newQualFileName)
                            if not outputType == 'all':
                                a.removeFastaAndQual(newFastaFileName, newQualFileName)
                                a.removeFastaAndQual(fastaBeforeTrim, qualBeforeTrim)
                    except FileNotFoundError as e:
                        usage(e, True)
                    except FileTypeError as e:
                        usage(e, False)
            elif scope == "locus":
                for folder in targetDirPathTuples:
                    try:
                        test = Folder(folder[1])
                        check = [x.endswith(".ab1") for x in test.walkThroughDir()]
                        if not set(check) == {False}:
                            filesList = [x for x in test.walkThroughDir() if x.endswith(".seq")]
                            a = Locus(folder[1], True)
                            initalLocusFasta = folder[0] + "/" + a.locusName + ".ALLINITIALSEQS.fa"
                            a.concatSeqFiles(filesList, initalLocusFasta)
                            b = Fasta(initalLocusFasta)
                            b.setMulti(True)
                            b.doAllFromFile()
                            try:
                                if outputType == 'fasta':
                                    a.handDirToTraceTunerAndCreateFasta(folder[0], windowSize, threshold)
                                    if args.summary:
                                        newFileName = folder[0] + "/" + a.locusName + ".TRIMMED.fa"
                                        beforeTrim = folder[0] + "/" + a.locusName + ".fa"
                                        summaryName = folder[0] + "/" + a.locusName + ".FASTASUMMARY" + ".csv"
                                        c = Fasta(beforeTrim)
                                        c.setMulti(True)
                                        c.doAllFromFile()
                                        d = Fasta(newFileName)
                                        d.setMulti(True)
                                        d.doAllFromFile()
                                        g= Overview(summaryName, b, c, d)
                                elif outputType == 'qual':
                                    a.handDirToTraceTunerAndCreateQual(folder[0], windowSize, threshold)
                                    if args.summary:
                                        newFileName = folder[0] + "/" + a.locusName + ".TRIMMED.qual"
                                        beforeTrim = folder[0] + "/" + a.locusName + ".qual"
                                        summaryName = folder[0] + "/" + a.locusName + ".QUALSUMMARY" + ".csv"
                                        c = Qual(beforeTrim)
                                        c.setMulti(True)
                                        c.doAllFromFile()
                                        d = Qual(newFileName)
                                        d.setMulti(True)
                                        d.doAllFromFile()
                                        g= Overview(summaryName, c ,d)
                                elif outputType == 'fastq' or outputType == 'all':
                                    try:
                                        a.handDirToTraceTunerAndCreateFasta(folder[0], windowSize, threshold)
                                        a.handDirToTraceTunerAndCreateQual(folder[0], windowSize, threshold)
                                        newFastaFileName = folder[0] + "/" + a.locusName + ".TRIMMED.fa"
                                        fastaBeforeTrim = folder[0] + "/" + a.locusName + ".fa"
                                        fastaAfterFilter = folder[0] + "/" + a.locusName + ".FILTERED.fa"
                                        newQualFileName = folder[0] + "/" + a.locusName + ".TRIMMED.qual"
                                        qualBeforeTrim = folder[0] + "/" + a.locusName + ".qual"
                                        qualAfterFilter = folder[0] + "/" + a.locusName + ".FILTERED.qual"
                                        a.writeFastq(newFastaFileName, newQualFileName)
                                        a.filterSequences(newFastaFileName, newQualFileName, fastaAfterFilter,
                                                          qualAfterFilter, minScore)
                                        if args.summary:
                                            fastaSummaryName = folder[0] + "/" + a.locusName + ".FASTASUMMARY" + ".csv"
                                            qualSummaryName = folder[0] + "/" + a.locusName + ".QUALSUMMARY" + ".csv"
                                            c1 = Fasta(fastaBeforeTrim)
                                            c1.setMulti(True)
                                            c1.doAllFromFile()
                                            c2 = Qual(qualBeforeTrim)
                                            c2.setMulti(True)
                                            c2.doAllFromFile()
                                            d1 = Fasta(newFastaFileName)
                                            d1.setMulti(True)
                                            d1.doAllFromFile()
                                            d2 = Qual(newQualFileName)
                                            d2.setMulti(True)
                                            d2.doAllFromFile()
                                            e1 = Fasta(fastaAfterFilter)
                                            e1.setMulti(True)
                                            e1.doAllFromFile()
                                            e2 = Qual(qualAfterFilter)
                                            e2.setMulti(True)
                                            e2.doAllFromFile()
                                            g = Overview(fastaSummaryName, b, c1, d1, e1)
                                            h = Overview(qualSummaryName, c2, d2, e2)
                                        if not outputType == 'all':
                                            a.removeFastaAndQual(newFastaFileName, newQualFileName)
                                            a.removeFastaAndQual(qualBeforeTrim, fastaBeforeTrim)
                                            a.removeFastaAndQual(fastaAfterFilter, qualAfterFilter)
                                    except FileNotFoundError as e:
                                        usage(e.args, True)
                            except CalledProcessError as e:
                                usage(e, True)
                    except (PermissionError, NotAbsolutePathError) as e:
                        usage(e, True)

        # Scenario 2B: User wants only the files that can be used from the given directory to be processed.
        # So subdirectories are excluded.
        if not args.recur:
            if scope == "trace":
                try:
                    dirContentList = folder.walkThroughDir()
                    for file in dirContentList:
                        try:
                            a = AB1(file)
                            oldFasta = file.split(".ab1")[0] + ".seq"  # Seq hardcoded suffix.
                            b = Fasta(oldFasta)
                            b.doAllFromFile()
                            if outputType == 'fasta':
                                a.handFileToTraceTunerAndCreateFasta(str(outputDir), windowSize, threshold)
                                if args.summary:
                                    newFileName = str(outputDir) + "/" + a.fileBaseName + ".TRIMMED.fa"
                                    beforeTrim = str(outputDir) + "/" + a.fileBaseName + ".fa"
                                    summaryName = str(outputDir) + "/" + a.fileBaseName + ".FASTASUMMARY" + ".csv"
                                    c = Fasta(beforeTrim)
                                    c.doAllFromFile()
                                    d = Fasta(newFileName)
                                    d.doAllFromFile()
                                    g= Overview(summaryName, b, c, d)
                            elif outputType == 'qual':
                                a.handFileToTraceTunerAndCreateQual(str(outputDir), windowSize, threshold)
                                if args.summary:
                                    newFileName = str(outputDir) + "/" + a.fileBaseName + ".TRIMMED.qual"
                                    beforeTrim = str(outputDir) + "/" + a.fileBaseName + ".qual"
                                    summaryName = str(outputDir) + "/" + a.fileBaseName + ".QUALSUMMARY" + ".csv"
                                    c = Qual(beforeTrim)
                                    c.doAllFromFile()
                                    d = Qual(newFileName)
                                    d.doAllFromFile()
                                    g = Overview(summaryName, c, d)
                            elif outputType == 'fastq' or outputType == 'all':
                                # Both a fasta and qual file have to be made first in order to make a fastq file.
                                a.handFileToTraceTunerAndCreateFasta(str(outputDir), windowSize, threshold)
                                a.handFileToTraceTunerAndCreateQual(str(outputDir), windowSize, threshold)
                                newFastaFileName = str(outputDir) + "/" + a.fileBaseName + ".TRIMMED.qual"
                                fastaBeforeTrim = str(outputDir) + "/" + a.fileBaseName + ".qual"
                                newQualFileName = str(outputDir) + "/" + a.fileBaseName + ".TRIMMED.qual"
                                qualBeforeTrim = str(outputDir) + "/" + a.fileBaseName + ".qual"
                                if args.summary:
                                    fastaSummaryName = str(outputDir) + "/" + a.fileBaseName + ".FASTASUMMARY" + ".csv"
                                    qualSummaryName = str(outputDir) + "/" + a.fileBaseName + ".QUALSUMMARY" + ".csv"
                                    c1 = Fasta(fastaBeforeTrim)
                                    c1.doAllFromFile()
                                    c2 = Qual(qualBeforeTrim)
                                    c2.doAllFromFile()
                                    d1 = Fasta(newFastaFileName)
                                    d1.doAllFromFile()
                                    d2 = Qual(newQualFileName)
                                    d2.doAllFromFile()
                                    g = Overview(fastaSummaryName, c1, d1)
                                    h = Overview(qualSummaryName, c2, d2)
                                a.writeFastq(newFastaFileName, newQualFileName)
                                if not outputType == 'all':
                                    a.removeFastaAndQual(newFastaFileName, newQualFileName)
                                    a.removeFastaAndQual(fastaBeforeTrim, qualBeforeTrim)
                        except (FileNotFoundError, PermissionError) as e:
                            usage(e, True)
                except FileTypeError as e:
                        usage(e, False)
            elif scope == "locus":
                try:
                    a = Locus(str(folder), True)
                    filesList = [x for x in a.walkThroughDir() if x.endswith(".seq")]
                    initalLocusFasta = str(folder) + "/" + a.locusName + ".ALLINITIALSEQS.fa"
                    a.concatSeqFiles(filesList, initalLocusFasta)
                    b = Fasta(initalLocusFasta)
                    b.setMulti(True)
                    b.doAllFromFile()
                    try:
                        if outputType == 'fasta':
                            a.handDirToTraceTunerAndCreateFasta(str(outputDir), windowSize, threshold)
                            if args.summary:
                                newFileName = str(folder) + "/" + a.locusName + ".TRIMMED.fa"
                                beforeTrim = str(folder) + "/" + a.locusName + ".fa"
                                summaryName = str(folder) + "/" + a.locusName + ".FASTASUMMARY" + ".csv"
                                c = Fasta(beforeTrim)
                                c.setMulti(True)
                                c.doAllFromFile()
                                d = Fasta(newFileName)
                                d.setMulti(True)
                                d.doAllFromFile()
                                g = Overview(summaryName, b, c, d)
                        elif outputType == 'qual':
                            a.handDirToTraceTunerAndCreateQual(str(outputDir), windowSize, threshold)
                            if args.summary:
                                newFileName = str(folder) + "/" + a.locusName + ".TRIMMED.qual"
                                beforeTrim = str(folder) + "/" + a.locusName + ".qual"
                                summaryName = str(folder) + "/" + a.locusName + ".QUALSUMMARY" + ".csv"
                                c = Qual(beforeTrim)
                                c.setMulti(True)
                                c.doAllFromFile()
                                d = Qual(newFileName)
                                d.setMulti(True)
                                d.doAllFromFile()
                                g= Overview(summaryName, c, d)
                        elif outputType == 'fastq' or outputType == 'all':
                            try:
                                a.handDirToTraceTunerAndCreateFasta(str(folder), windowSize, threshold)
                                a.handDirToTraceTunerAndCreateQual(str(folder), windowSize, threshold)
                                newFastaFileName = str(folder) + "/" + a.locusName + ".TRIMMED.fa"
                                fastaBeforeTrim = str(folder) + "/" + a.locusName + ".fa"
                                fastaAfterFilter = str(folder) + "/" + a.locusName + ".FILTERED.fa"
                                newQualFileName = str(folder) + "/" + a.locusName + ".TRIMMED.qual"
                                qualBeforeTrim = str(folder) + "/" + a.locusName + ".qual"
                                qualAfterFilter = str(folder) + "/" + a.locusName + ".FILTERED.qual"
                                a.writeFastq(newFastaFileName, newQualFileName)
                                a.filterSequences(newFastaFileName, newQualFileName, fastaAfterFilter, qualAfterFilter,
                                                  minScore)
                                if args.summary:
                                    fastaSummaryName = str(folder) + "/" + a.locusName + ".FASTASUMMARY" + ".csv"
                                    qualSummaryName = str(folder) + "/" + a.locusName + ".QUALSUMMARY" + ".csv"
                                    c1 = Fasta(fastaBeforeTrim)
                                    c1.setMulti(True)
                                    c1.doAllFromFile()
                                    c2 = Qual(qualBeforeTrim)
                                    c2.setMulti(True)
                                    c2.doAllFromFile()
                                    d1 = Fasta(newFastaFileName)
                                    d1.setMulti(True)
                                    d1.doAllFromFile()
                                    d2 = Qual(newQualFileName)
                                    d2.setMulti(True)
                                    d2.doAllFromFile()
                                    e1 = Fasta(fastaAfterFilter)
                                    e1.setMulti(True)
                                    e1.doAllFromFile()
                                    e2 = Qual(qualAfterFilter)
                                    e2.setMulti(True)
                                    e2.doAllFromFile()
                                    g = Overview(fastaSummaryName, c1, d1, e1)
                                    h = Overview(qualSummaryName, d2, d2, e2)
                                if not outputType == 'all':
                                    a.removeFastaAndQual(newFastaFileName, newQualFileName)
                                    a.removeFastaAndQual(qualBeforeTrim, fastaBeforeTrim)
                                    a.removeFastaAndQual(fastaAfterFilter, qualAfterFilter)
                            except FileNotFoundError as e:
                                usage(e.args, True)
                    except CalledProcessError as e:
                        usage(e, True)
                except (PermissionError, NotAbsolutePathError) as e:
                    usage(e, True)
    if args.stats:
        ParseSummaries.main(windowSize, threshold, minScore, str(outputDir), str(outputDir))


def usage(msg, abort=False):
    """function that prints the docstring and an error message."""
    if msg is str:
        msg = msg[len(msg)-1]
    print(__doc__ + "\n" + "Error: " + str(msg), file=sys.stderr)
    if abort:
        sys.exit()

if __name__ == "__main__":
    main()
    print("Finished")