Use case description of the sequencecheckers pipeline:

The user calls the pipeline as a python commandline executable. The user can invoke a help message and program description.
If the user wants to call the programming he has multiple options available to him. Combinations between options are possible.
The user gets a good error message when a combination of commandline options is not possible.
Input is provided in the form of a single AB1 file or a directory containing these AB1 files. Given input is checked and lethal exceptions kill the process.
Non lethal exceptions should be handled properly in order for the program to continue and provide clearance in a log.
Program should give a message on finalizing the execution.

Triggers:
- Commandline call of user

Actors:
- User

Preconditions:
- Machine needs to have tracetuner installed with "ttuner" in its $PATH
- Biopython module dependant.

Goals (succes conclusion):
- The program delivers the requested output at the requested place.

Exceptions (failed conclusion):
- User does not give a possible combination of commandline arguments and/or options.
- User delivers wrong input file type.
- User delivers corrupt file of right file type.
- User demands the program to place the output on an impossible location.
- Tracetuner is not installed in the $PATH variable.
- User does not supply absolute paths as arguments.


# A. Per sequence
#
# a) 1 fasta file per sequence (The name of the file should include at least the name of the locus and the individual ID) [Done]
# b) 1 fastaq file per sequence (Idem) [Done]
#
#
# B. Per locus
#
# c) 1 alignment file per locus in fasta format with the full sequences (The name of the file should include the name of the locus) [Done]
# d) 1 alignment file per locus with the full sequences and quality scores. Each base should contain the quality score (The name of the file should include the name of the locus)
#
# Based on the alignment file with the quality score we should be able to identify and extract the columns that contain SNPs
#
# e) 1 file with all the columns that contain SNPs (Inside the file we should be able to identified for each SNPs, the name of the locus, the position of the SNPs based on the alignment and the quality score.
# The name of the locus and position could be on the header and the quality score another column)
#
#
# C. Global
#
# f) 1 file that combines all the SNPs extracted per locus. The idea of this file is to summarize all the available SNPs. This file will then be used to extract the SNPs for further analyses.
# * If the SNPs are called with the IUPAC code we could also make an additional file with the SNPs in the other format: (including two base pairs, such as CT instead of Y. In some programs that is the format we need, but we can check that later.
#
#
# The masked file, If I am not wrong was used on Per's pipeline to change all the ambiguities to "N". In our case we have to make the distinction of these ambiguities with the SNPs and mask all the ambiguities with low quality scores that are because of error rates or false SNPs and leave the SNPs.
