-- If tables exist or procedure, drop it to make sure there is no interference.
-- ATTENTION! The order of both deletion and creation is important because some tables depend on others and need to have a reference.
drop table if exists Heterozygous_position;
drop table if exists Chromatogram;
drop table if exists Individual;
drop table if exists Locus_part;
drop table if exists Population;
drop table if exists Locus;

-- Create tables and set keys and references.
create table Locus
(   id                int     not null    unique  auto_increment,
    originial_name    text    null,
    data_name         text    null,
    primary key(id)
);

create table Population
(   id                      int     not null    unique auto_increment,
    name                    text    not null,
    abbreviation            text    not null,
    number_of_individuals   int     not null,
    region                  text    null,
    primary key(id)
);

create table Locus_part
(   id              int         not null    unique  auto_increment,
    locus_id        int         not null,
    part_number     int         not null,
    name            text        not null,
    reference       text        not null,
    foreign key (locus_id) references Locus(id),
    primary key(id)
);

create table Individual
(   id              int     not null    unique  auto_increment,
    population_id   int     not null,
    original_code   text    null,
    data_code       text    null,
    foreign key (population_id) references Population(id),
    primary key(id)
);

create table Chromatogram
(   id                  int         not null    unique  auto_increment,
    individual_id       int         not null,
    part_id             int         not null,
    sequence            text        not null,
    filename            text        not null,
    number_of_het_pos   int     not null,
    foreign key (individual_id) references Individual(id),
    foreign key (part_id) references Locus_part(id),
    primary key(id)
);

create table Heterozygous_position
(   id                      int         not null    unique  auto_increment,
    chromatogram_id         int         not null,
    reference_nucleotide    char(1)     not null,
    Iupac                   char(1)     not null,
    quality_score           int         not null,
    foreign key (chromatogram_id) references Chromatogram(id),
    primary key(id)
);

